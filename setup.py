#!/usr/bin/env python

from distutils.core import setup

setup(
        name = 'scivis',
        version = '0.2',
        description = 'Scientific Visualization Scripts',
        author = 'David Radice',
        author_email = 'dradice@astro.princeton.edu',
        license = 'GPLv3',
        packages = ['scivis',
                    'scivis.cli',
                    'scivis.cfg',
                    'scivis.data',
                    'scivis.fields',
                    'scivis.mod',
                    'scivis.vis'],
        package_data = {'scivis' : ['cfg/*.cfg', 'mod/*.mod']},
        requires = ['matplotlib', 'scidata'],
        scripts = [
            './bin/scivis',
            './bin/scivis-h5-extract',
            './bin/scivis-h5-repack',
            './bin/scivis-ffmpeg'
            ],
        url = 'https://bitbucket.org/dradice/scivis'
)
