scivis
======

Scripts for visualizing [Cactus](http://cactuscode.org/)/
[Carpet](http://carpetcode.org/) simulations

Requirements
------------

* [matplotlib](https://matplotlib.org/)
* [h5py](https://www.h5py.org/)
* [numpy](http://www.numpy.org/)
* [scipy](https://www.scipy.org/)
* [scidata](https://bitbucket.org/dradice/scidata)

Needs a python 2.6+ or 3.3+ installation. Note that the [python anaconda distribution](https://www.anaconda.com/download)
already includes most of dependencies needed by scivis (except for scidata).
Installing anaconda is the recommended way to setup a python stack for scivis.

Installation
------------

After all the required packages are installed, scivis can be installed using
the following commands

~~~
    $ git clone https://bitbucket.org/dradice/scivis.git
    $ cd scivis
~~~

To install scivis for all users (this requires administrative privileges) run

~~~
    $ python setup.py install
~~~

Otherwise scivis can be installed in the $HOME directory using the command

~~~
    $ python setup.py install --user
~~~

If scivis is installed in the `$HOME` directory, then it is necessary to add
`$HOME/Library/Python/2.7/bin` (MacOS), or `$HOME/.local/bin` (Linux) to the
system PATH. Similarly, it will also be necessary to add
`$HOME/Library/Python/2.7/lib/python/site-packages` (MacOS), or
`$HOME/.local/lib/python2.7/site-packages` (Linux) to the `PYTHONPATH`
environment variable For example, when running bash on Linux it is necessary to
add

~~~
    export PATH=$PATH:$HOME/.local/bin
    export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python2.7/site-packages
~~~

to the `.bashrc` file to enable scivis. Please note, if you used a python3 to install, change python2.7 to python3.7 or appropiate path 
where it's installed on the system while exporting. 

Usage
-----

Change to the root folder of the simulation to be visualized

~~~
    $ cd /path/to/simulation
~~~

Initialize scivis

~~~
    $ scivis init
~~~

This will create a folder called `analysis`. `scivis init` will initialize the
analysis directory structure, but will not install any visualization script. To
be able to visualize any quantity from your simulation you will need to install
the appropriate visualization scripts. This can be done with the command

~~~
    $ scivis install scriptname
~~~

It is also possible to install entire groups of scripts at once with

~~~
    $ scivis install @groupname
~~~

A list of all of the available visualization scripts and groups can be obtained
with the command

~~~
    $ scivis list
~~~

Before running `scivis` it is a good idea to edit the `config/base.py` to setup
the parameters for the visualization. Please refer to the comments inside this
file for a description of each parameter. Script specific parameters can also
be adjusted by editing the appropriate files in the `config` directory. For
example, to change the setup for the density plots edit `config/rho.py`.

Once all options are set, it is possible to run using "make" from the
`analysis` folder:

~~~
    $ cd analysis && make
~~~
