import abc
import numpy as np

import scidata.carpet.hdf5 as h5
import scivis
import scivis.plot
import scivis.units as units

from scivis.fields import ScalarField
from math import pi, sqrt
from scidata.utils import locate

""" Static CarpetIOHDF5 array database """
class CarpetHDF5AR(object):
    """
    * dataset : scidata.carpet.hdf5 dataset
    * files   : list of opened hdf5 files
    * root    : datapath
    * fields  : registered fields
    """
    dataset = None
    files   = set([])
    root    = "."
    fields  = None

    griddb  = None

    @staticmethod
    def clean():
        CarpetHDF5AR.dataset = None
        CarpetHDF5AR.files   = set([])
        CarpetHDF5AR.root    = "."
        CarpetHDF5AR.fields  = []

    @staticmethod
    def init(root = '.'):
        CarpetHDF5AR.dataset = None
        CarpetHDF5AR.files   = set([])
        CarpetHDF5AR.root    = root
        CarpetHDF5AR.fields  = []

    @staticmethod
    def register(field):
        L = []
        for f in field.filenames:
            L += locate("{0}.*.h5".format(f), root=CarpetHDF5AR.root,
                    followlinks=True)
        CarpetHDF5AR.files = CarpetHDF5AR.files.union(set(L))
        CarpetHDF5AR.fields.append(field)

    @staticmethod
    def read():
        CarpetHDF5AR.dataset = h5.dataset(CarpetHDF5AR.files)
        for f in CarpetHDF5AR.fields:
            f.init()
        CarpetHDF5AR.griddb = [None, None]

    @staticmethod
    def get_reflevel(itern):
        if CarpetHDF5AR.griddb[0] != itern:
            CarpetHDF5AR.griddb[0] = itern
            CarpetHDF5AR.griddb[1] = \
                CarpetHDF5AR.dataset.get_reflevel(iteration=itern)
        return CarpetHDF5AR.griddb[1]

""" Cactus/Carpet coordiantes  """
class CarpetCoordinatesAR(object):
    @abc.abstractmethod
    def get_coord(self, itern):
        """
        Get the coordinates of the grid points

        * itern : iteration number
        """
        pass
    @abc.abstractmethod
    def get_cells(self, itern):
        """
        Get the coordinates of the edges of the grid cells (the dual grid)

        * itern : iteration number
        """
        pass

    def get_label(self, axis):
        return self.get_labels()[axis]
    @abc.abstractmethod
    def get_labels(self):
        pass

    def transf_scalar(self, func):
        """
        Apply symmetries / coordinate transformations
        """
        return func
    def transf_vector(self, vec):
        """
        Apply symmetries / coordinate transformations
        """
        return vec

""" Coordinates used in THC_Leakage """
class THCLeakageCoordinates(CarpetCoordinatesAR):
    def __init__(self):
        if scivis.options["base.units"] == "cactus":
            unit = r"M_\odot"
        else:
            unit = r"{\rm km}"

        self.plane = None
        self.label = {}
        self.label = {}
        for plane in ["xy", "xz", "yz"]:
            self.label[plane] = {}
            for i, axis in enumerate(["x", "y"]):
                lbl = scivis.options["plot.label.{0}.{1}".format(plane, axis)]
                if lbl is None:
                    self.label[plane][axis] = r"${}\ [{}]$".format(
                            plane[i], unit)
                else:
                    self.label[plane][axis] = lbl.replace("@coord@", plane[i])
        self.rmax = scivis.options["array.coordinates.rmax"]
        self.grid = None

    def __get_coord_or_cells(self, dual):
        if dual:
            nextra = 1
            shift  = -0.5
        else:
            nextra = 0
            shift  = 0.0

        if scivis.options['base.units'] == 'metric':
            f = units.conv_length(units.cactus, units.metric, 1.0)/1000.0
        else:
            f = 1.0
        if self.plane is None:
            rad, phi, theta = np.mgrid[0:self.nrad+nextra, 0:self.nphi+nextra,\
                    0:self.ntheta+nextra].astype(np.float32)
            rad = (rad + shift) * self.rmax/(self.nrad - 1)
            phi = (phi + shift) * (2*pi)/(self.nphi - 1)
            theta = (theta + shift) * pi/(self.ntheta - 1)
            x = f * rad * np.cos(phi) * np.sin(theta)
            y = f * rad * np.sin(phi) * np.sin(theta)
            z = f * rad * np.cos(theta)
            return x, y, z
        if self.plane == "xy":
            rad, phi = np.mgrid[0:self.nrad+nextra,\
                    0:self.nphi+nextra].astype(np.float32)
            rad = (rad + shift) * self.rmax/(self.nrad - 1)
            phi = (phi + shift) * (2*pi)/(self.nphi - 1)
            x = f * rad * np.cos(phi)
            y = f * rad * np.sin(phi)
            return x, y
        if self.plane == "xz" or self.plane == "yz":
            rad, theta = np.mgrid[0:self.nrad+nextra,\
                    0:2*self.ntheta+nextra-1].astype(np.float32)
            rad = (rad  + shift) * self.rmax/(self.nrad - 1)
            theta = (theta + shift) * pi/(self.ntheta - 1)
            x = f * rad * np.sin(theta)
            z = f * rad * np.cos(theta)
            return x, z
        raise Exception("This is a bug in the code")
    def get_coord(self, itern):
        """
        Get the coordinates of the grid points

        * itern : iteration number
        """
        return self.__get_coord_or_cells(False)
    def get_cells(self, itern):
        """
        Get the coordinate of the grid cells vertices (the dual grid)

        * itern : iteration number
        """
        return self.__get_coord_or_cells(True)

    def get_labels(self):
        """
        Get the axis labels
        """
        if self.plane is None:
            return {"x": "x", "y": "y", "z": "z"}
        assert self.plane in ["xy", "xz", "yz"]
        return self.label[self.plane]

    def set_plane(self, plane):
        """
        Set the current plane
        """
        if plane is None:
            self.plane = None
        else:
            assert plane in ["xy", "xz", "yz"]
            self.plane = plane

    def read_grid(self, itern):
        grid        = CarpetHDF5AR.get_reflevel(itern)
        self.nrad   = grid.n[0]
        self.ntheta = int(round(sqrt(float(grid.n[1]//2))))
        self.nphi   = 2*self.ntheta
        if self.ntheta*self.nphi != grid.n[1]:
            raise ValueError("The leakage grid is inconsistent")

    def transf_scalar(self, func):
        """
        Apply symmetries / coordinate transformations
        """
        fnew = func.reshape((self.nrad, self.nphi, self.ntheta))
        if self.plane is None:
            return fnew
        if self.plane == "xy":
            out = np.empty((self.nrad, self.nphi), dtype=fnew.dtype)
            out[:] = np.NAN
            if 0 != self.ntheta % 2:
                out[:,:] = fnew[:,:, self.ntheta//2]
            else:
                itheta = self.ntheta//2
                out[:,:] = 0.5*(fnew[:,:, itheta-1] + fnew[:,:, itheta])
        elif self.plane == "xz":
            out = np.empty((self.nrad, 2*self.ntheta-1), dtype=fnew.dtype)
            out[:] = np.NAN
            out[:, :self.ntheta] = fnew[:, 0,:]
            iphi = self.nphi//2
            out[:, self.ntheta:] = 0.5*(fnew[:, iphi-1, -2::-1] + \
                    fnew[:, iphi, -2::-1])
        elif self.plane == "yz":
            out = np.empty((self.nrad, 2*self.ntheta-1), dtype=fnew.dtype)
            out[:] = np.NAN
            iphi1 = self.nphi//4
            iphi2 = 3*iphi1
            out[:, :self.ntheta] = 0.5*(fnew[:, iphi1+1,:] + fnew[:, iphi1,:])
            out[:, self.ntheta:] = 0.5*(fnew[:, iphi2+1, -2::-1] + \
                    fnew[:, iphi2, -2::-1])
        else:
            raise Exception("This is a bug in the code")
        return np.ma.masked_invalid(out)

    def transf_vector(self, vec):
        """
        Apply symmetries / coordinate transformations

        WARNING: this only reshapes the vector components and does not
        handle the coordinate transformation
        """
        return tuple([transf_scalar(vc) for vc in vec])

""" Scalar field array data """
class ScalarDataAR(object):
    def __init__(self, field):
        assert isinstance(field, ScalarField)
        self.filenames = field.filenames
        self.field     = field
        self.label     = field.label
    def get_data(self, itern):
        """
        Get the array data as a masked array

        * itern : iteration number
        """
        grid = CarpetHDF5AR.get_reflevel(itern)

        rdata    = np.empty(grid.n)
        rdata[:] = np.NAN
        for comp in grid.components:
            flist = []
            for var in self.field.fieldnames:
                flist.append(CarpetHDF5AR.dataset.get_component_data(var,
                    itern, 0, comp.id[0], comp.id[1]))
            comp.restrict(rdata)[:] = self.field.compute(flist)

        return np.ma.masked_invalid(rdata)
    def init(self):
        pass
