import numpy as np

import scivis
import scivis.units as units
from scivis.utils import Ellipsoid, Ellipse

from scidata.utils import ilocate

""" Position and approximate shape of the BH horizon """
class BHDiagnostics(object):
    """
    * horizon: shape of the BH horizon {it: Ellipsoid}

    Note: at the moment only 1 horizon is supported
    """
    horizon = {}

    @staticmethod
    def clean():
        BHDiagnostics.horizon = {}
    @staticmethod
    def init(root='.'):
        for fname in ilocate("BH_diagnostics.ah1.gp",
                root=root, followlinks=True):
            with open(fname, "r") as ifile:
                for line in ifile:
                    if line[0] == '#':
                        continue
                    dline = line.split()
                    it = int(dline[0])
                    if it in BHDiagnostics.horizon:
                        continue
                    center = (float(dline[i]) for i in [2, 3, 4])
                    diam = (float(dline[i+1]) - float(dline[i])
                            for i in [14, 16, 18])
                    BHDiagnostics.horizon[it] = Ellipsoid(center, diam)

""" BH horizon """
class BHHorizon(object):
    def get_shapes(self, plane, it):
        """
        Get the shape of the horizon at the current iteration.

        If there is no horizon, this returns None
        """
        axis = {"xy": 2, "xz": 1, "yz": 0}[plane]
        if it in BHDiagnostics.horizon:
            horizons = []
            hor = BHDiagnostics.horizon[it].slice(s=0., axis=axis)
            if hor is not None:
                horizons.append(hor)
                if plane[1] == "z" and \
                        "reflecting_xy" in scivis.options["base.symmetries"] and \
                        hor.center[1] > 1e-2*hor.diam[1]:
                    center = (hor.center[0], -hor.center[1])
                    diam = hor.diam
                    horizons.append(Ellipse(center, diam))
            return horizons
        else:
            return []
    def transf_shape(self, shape):
        """
        Apply coordinate transformations
        """
        if shape is None:
            return None
        if scivis.options["base.units"] == "metric":
            f = units.conv_length(units.cactus, units.metric, 1.0)/1000.0
        else:
            f = 1.0
        return shape.scale(f)
