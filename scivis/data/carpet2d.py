import abc
import numpy as np

import scidata.carpet.hdf5 as h5
import scidata.kernels
import scipy.signal as signal
import scivis
import scivis.plot
import scivis.units as units
import sys

from scivis.fields import ScalarField, VectorField
from math import pi
from scidata.utils import locate

""" Static CarpetHDF5 2D database """
class CarpetHDF52D(object):
    """
    * dataset : scidata.carpet.hdf5 dataset
    * files   : list of opened hdf5 files
    * plane   : selected plane
    * rlevels : available refinement levels
    * root    : datapath

    * fields  : registered fieldsJ
    """
    dataset = None
    files   = set([])
    plane   = ""
    rlevels = []
    root    = "."

    griddb  = None

    fields  = None

    @staticmethod
    def clean():
        CarpetHDF52D.dataset = None
        CarpetHDF52D.files   = set([])
        CarpetHDF52D.plane   = ""
        CarpetHDF52D.rlevels = []
        CarpetHDF52D.root    = "."

        CarpetHDF52D.fields  = []

    @staticmethod
    def init(plane = 'xy', root = '.'):
        assert plane in ["xy", "xz", "yz"]

        CarpetHDF52D.dataset = None
        CarpetHDF52D.files   = set([])
        CarpetHDF52D.plane   = plane
        CarpetHDF52D.rlevels = []
        CarpetHDF52D.root    = root

        CarpetHDF52D.fields  = []

    @staticmethod
    def register(field):
        L = []
        for f in field.filenames:
            L += locate("{0}.{1}.h5".format(f, CarpetHDF52D.plane),
                    root=CarpetHDF52D.root, followlinks=True)

        CarpetHDF52D.files = CarpetHDF52D.files.union(set(L))

        CarpetHDF52D.fields.append(field)

    @staticmethod
    def read():
        CarpetHDF52D.dataset = h5.dataset(sorted(list(CarpetHDF52D.files)))

        CarpetHDF52D.rlevels = sorted(list(set(
            CarpetHDF52D.dataset.select_reflevels())))
        CarpetHDF52D.griddb = [[None, None] for i in CarpetHDF52D.rlevels]

        for f in CarpetHDF52D.fields:
            f.init()

    @staticmethod
    def get_reflevel(rlidx, itern):
        if CarpetHDF52D.griddb[rlidx][0] != itern:
            CarpetHDF52D.griddb[rlidx][0] = itern
            CarpetHDF52D.griddb[rlidx][1] = \
                CarpetHDF52D.dataset.get_reflevel(reflevel=rlidx,
                        iteration=itern)
        return CarpetHDF52D.griddb[rlidx][1]

""" Cactus/Carpet coordiantes  """
class CarpetCoordinates2D(object):
    def get_coord(self, itern):
        """
        Get the data on the whole grid

        * itern : iteration number
        """
        out = [None for i in range(CarpetHDF52D.rlevels[-1] + 1)]
        rlevels = list(CarpetHDF52D.dataset.select_reflevels(iteration=itern))
        for r in rlevels:
            out[r] = CarpetHDF52D.get_reflevel(r, itern).mesh()
        return out
    def get_cells(self, itern):
        """
        Get the location of the grid vertices (the dual grid)

        * itern : iteration number
        """
        out = [None for i in range(CarpetHDF52D.rlevels[-1] + 1)]
        rlevels = list(CarpetHDF52D.dataset.select_reflevels(iteration=itern))
        for r in rlevels:
            out[r] = CarpetHDF52D.get_reflevel(r, itern).dual()
        return out

    def get_label(self, axis):
        return self.get_labels()[axis]
    @abc.abstractmethod
    def get_labels(self):
        pass

    def transf_coord(self, coord):
        """
        Apply symmetries / coordinate transformations
        """
        return coord
    def transf_scalar(self, coord, func):
        """
        Apply symmetries / coordinate transformations
        """
        return func
    def transf_vector(self, coord, vec):
        """
        Apply symmetries / coordinate transformations
        """
        return vec

""" Cartesian coordinates """
class CartesianCoordinates2D(CarpetCoordinates2D):
    def __init__(self):
        if scivis.options["base.units"] == "cactus":
            unit = r"M_\odot"
        else:
            unit = r"{\rm km}"

        self.label = {}
        for plane in ["xy", "xz", "yz"]:
            self.label[plane] = {}
            for i, axis in enumerate(["x", "y"]):
                lbl = scivis.options["plot.label.{0}.{1}".format(plane, axis)]
                if lbl is None:
                    self.label[plane][axis] = r"${}\ [{}]$".format(
                            plane[i], unit)
                else:
                    self.label[plane][axis] = lbl.replace("@coord@", plane[i])

    def get_labels(self):
        """
        Get the axis labels
        """
        return self.label[CarpetHDF52D.plane]

    def make_reflxy_grid(self, coord):
        """
        Setup the extended grid to be used to apply the reflecting symmetry

        Returns x0, x1, dx, nx, z0, z1, dz, nz
        """
        x  = coord[0][:, 0]
        z  = coord[1][0,:]

        x1 = x.max()
        x0 = x.min()
        dx = x[1] - x[0]
        nx = x.shape[0]

        z1 = z.max()
        z0 = -z1
        dz = z[1] - z[0]
        nz = int(round((z1 - z0)/dz) + 1)

        return x0, x1, dx, nx, z0, z1, dz, nz

    def make_reflxy_range(self, coord):
        """
        Creates indices range

        The data in the old indexing system is in the range
                iz0:iz1 (z)
        The data in the new indexing system is in the ranges
                jz0:jz1 (Img of iz1-1:iz0-1:-1)
                jz2:jz3 (Img of iz0:iz1)
        The total size of the new array is jz3

        Note that
                ?z0     == 0
                iz0:iz1 == :
        This method returns
                jz1, jz2, jz3
        """
        x0, x1, dx, nx, z0, z1, dz, nz = self.make_reflxy_grid(coord)
        jz1 = coord[0].shape[1]
        jz2 = nz - coord[0].shape[1]
        jz3 = nz
        return jz1, jz2, jz3

    def make_reflxy_mesh(self, coord):
        """
        Create an extended grid to be used to apply the reflecting symmetry
        """
        x0, x1, dx, nx, z0, z1, dz, nz = self.make_reflxy_grid(coord)
        msh = np.mgrid[0:nx, 0:nz]*1.0
        return x0 + dx*msh[0], z0 + dz*msh[1]

    def make_rot180_grid(self, coord):
        """
        Setup the extended grid to be used to apply the rotating symmetry

        Returns x0, x1, dx, nx, y0, y1, dy, ny
        """
        x = coord[0][:, 0]
        y = coord[1][0,:]

        x1 = x.max()
        x0 = -x1
        dx = x[1] - x[0]
        nx = int(round((x1 - x0)/dx) + 1)

        y1 = max(y.max(), -y.min())
        y0 = min(y.min(), -y.max())
        dy = y[1] - y[0]
        ny = int(round((y1 - y0)/dy) + 1)

        return x0, x1, dx, nx, y0, y1, dy, ny

    def make_rot180_range(self, coord):
        """
        Create indices ranges

        The data in the old indexing system is in the range
                ix0:ix1 (x)
                iy0:iy1 (y)
        The data in the new indexing system is in the ranges
                jx0:jx1 (Img of ix1-1:ix0-1:-1) and jx2:jx3 (Img of ix0:ix1)
                jy0:jy1 (img of iy1-1:iy0-1:-1) and jy3 (siz in the y-direction)
        The total size of the new arrays is ix3, jx3

        Note that
                jx0 = 0
                i?0:i?1 == ::

        This methods returns
                jx1, jx2, jx3, jy0, jy1, jy2, jy3
        """
        x0, x1, dx, nx, y0, y1, dy, ny = self.make_rot180_grid(coord)

        y     = coord[1][0,:]
        siz_x = coord[0].shape[0]
        siz_y = coord[0].shape[1]

        jx1 = siz_x
        jx2 = nx - siz_x
        jx3 = nx

        # The box in x > 0 is "higher" than the one in x < 0
        if y.max() == y1:
            jy0 = 0
            jy1 = siz_y
            jy2 = ny - siz_y
            jy3 = ny
        else:
            jy2 = 0
            jy3 = siz_y
            jy0 = ny - siz_y
            jy1 = ny

        return jx1, jx2, jx3, jy0, jy1, jy2, jy3

    def make_rot180_mesh(self, coord):
        """
        Create an extended grid to be used to apply the rotating symmetry
        """
        x0, x1, dx, nx, y0, y1, dy, ny = self.make_rot180_grid(coord)
        msh = np.mgrid[0:nx, 0:ny] * 1.0
        return x0 + dx*msh[0], y0 + dy*msh[1]

    def transf_coord(self, coord):
        """
        Apply symmetries / coordinate transformations

        * coord : the *original* untransformed coordinates
        """
        x, y = coord

        if scivis.options['base.units'] == 'metric':
            f = units.conv_length(units.cactus, units.metric, 1.0)/1000.0
        else:
            f = 1.0

        if 'reflecting_xy' in scivis.options['base.symmetries']:
            if CarpetHDF52D.plane[1] == "z":
                x, y = self.make_reflxy_mesh((x, y))
        if 'rotating_180' in scivis.options['base.symmetries']:
            if CarpetHDF52D.plane == 'xy' or CarpetHDF52D.plane == 'xz':
                x, y = self.make_rot180_mesh((x, y))

        return f*x, f*y

    def transf_scalar(self, coord, func):
        """
        Apply symmetries / coordinate transformations

        * coord : the *original* untransformed coordinates
        """
        x, y = coord
        if 'reflecting_xy' in scivis.options['base.symmetries']:
            if CarpetHDF52D.plane[1] == "z":
                jz1, jz2, jz3 = self.make_reflxy_range((x, y))

                fnew             = np.empty((x.shape[0], jz3))
                fnew[:]          = np.NAN
                fnew[:,    :jz1] = func[:, ::-1]
                fnew[:, jz2:jz3] = func[:,:]

                x, y = self.make_reflxy_mesh((x, y))
                func = np.ma.masked_invalid(fnew)
        if 'rotating_180' in scivis.options['base.symmetries']:
            if CarpetHDF52D.plane == "xy" or CarpetHDF52D.plane == "xz":
                jx1, jx2, jx3, jy0, jy1, jy2, jy3 = \
                        self.make_rot180_range((x, y))

                fnew                   = np.empty((jx3, max(jy1, jy3)))
                fnew[:]                = np.NAN
                fnew[   :jx1, jy0:jy1] = func[::-1, ::-1]
                fnew[jx2:jx3, jy2:jy3] = func[:,:]

                x, y = self.make_rot180_mesh((x, y))
                func = np.ma.masked_invalid(fnew)
        return func

    def transf_vector(self, coord, vec):
        """
        Apply symmetries / coordinate transformations

        * coord : the *original* untransformed coordinates
        """
        x, y   = coord
        vx, vy = vec
        if 'reflecting_xy' in scivis.options['base.symmetries']:
            if CarpetHDF52D.plane[1] == 'z':
                jz1, jz2, jz3 = self.make_reflxy_range((x, y))

                vxnew = np.empty((x.shape[0], jz3))
                vynew = np.empty(vxnew.shape)
                msnew = np.empty(vxnew.shape, dtype='bool')

                vxnew[:] = np.NAN
                vynew[:] = np.NAN
                msnew[:] = True

                vxnew[:,    :jz1] = vx[:, ::-1]
                vxnew[:, jz2:jz3] = vx
                vynew[:,    :jz1] = - vy[:, ::-1]
                vynew[:, jz2:jz3] = vy
                msnew[:,    :jz1] = vx.mask[:, ::-1]
                msnew[:, jz2:jz3] = vx.mask

                x, y = self.make_reflxy_mesh((x, y))
                vx = np.ma.masked_array(vxnew, mask=msnew)
                vy = np.ma.masked_array(vynew, mask=msnew)
        if 'rotating_180' in scivis.options['base.symmetries']:
            if CarpetHDF52D.plane == "xy" or CarpetHDF52D.plane == "xz":
                jx1, jx2, jx3, jy0, jy1, jy2, jy3 = \
                        self.make_rot180_range((x, y))

                vxnew = np.empty((jx3, max(jy1, jy3)))
                vynew = np.empty(vxnew.shape)
                msnew = np.empty(vxnew.shape, dtype='bool')

                vxnew[:] = np.NAN
                vynew[:] = np.NAN
                msnew[:] = True

                vxnew[   :jx1, jy0:jy1] = - vx[::-1, ::-1]
                vxnew[jx2:jx3, jy2:jy3] = vx
                vynew[   :jx1, hy0:jy1] = - vy[::-1, ::-1]
                vynew[jx2:jx3, jy2:jy3] = vy
                msnew[   :jx1, jy0:jy1] = vx.mask[::-1, ::-1]
                msnew[jx2:jx3, jy2:jy3] = vx.mask

                x, y = self.make_reflxy_mesh((x, y))
                vx = np.ma.masked_array(vxnew, mask=msnew)
                vy = np.ma.masked_array(vynew, mask=msnew)
        return vx, vy

"""
Spherical coordinates

x, y, z as read from the data are interpreted as r, phi, theta - pi/4.
When plotting the "xy" and "xz" planes this class convertes from spherical to
cartesian coordinates. When plotting the "yz" "plane" the coordinates are
converted to angular coordinates theta, phi.
"""
class SphericalCoordinates2D(CarpetCoordinates2D):
    def __init__(self):
        if scivis.options["base.units"] == "cactus":
            unit = r"M_\odot"
        else:
            unit = r"{\rm km}"

        self.label = {}
        for plane in ["xy", "xz"]:
            self.label[plane] = {}
            for i, axis in enumerate(["x", "y"]):
                lbl = scivis.options["plot.label.{0}.{1}".format(plane, axis)]
                if lbl is None:
                    self.label[plane][axis] = r"${}\ [{}]$".format(
                            plane[i], unit)
                else:
                    self.label[plane][axis] = lbl.replace("@coord@", plane[i])

        self.label["yz"] = {}
        lbl = scivis.options["plot.label.yz.x"]
        if lbl is None:
            self.label["yz"]["x"] = r"$\phi$"
        else:
            self.label["yz"]["x"] = lbl.replace("@coord@", r"\phi")

        lbl = scivis.options["plot.label.yz.y"]
        if lbl is None:
            self.label["yz"]["y"] = r"$\theta$"
        else:
            self.label["yz"]["y"] = lbl.replace("@coord@", r"\theta")

    def get_labels(self):
        """
        Get the axis labels
        """
        return self.label[CarpetHDF52D.plane]

    def transf_coord(self, coord):
        """
        Apply symmetries / coordinate transformations

        * coord : the *original* untransformed coordinates
        """
        if scivis.options['base.units'] == 'metric':
            f = units.conv_length(units.cactus, units.metric, 1.0)/1000.0
        else:
            f = 1.0

        if CarpetHDF52D.plane == "xy":
            r   = coord[0]
            phi = coord[1]
            x   = r*np.cos(phi)
            y   = r*np.sin(phi)
            return f*x, f*y
        elif CarpetHDF52D.plane == "xz":
            r     = coord[0]
            theta = coord[1] + pi/4
            x     = r*np.sin(theta)
            z     = r*np.cos(theta)
            return f*x, f*z
        elif CarpetHDF52D.plane == "yz":
            return coord

    def transf_vector(self, coord, vec):
        """
        Apply symmetries / coordinate transformations

        * coord : the *original* untransformed coordinates
        """
        if CarpetHDF52D.plane == "xy":
            r    = coord[0]
            phi  = coord[1]
            vr   = vec[0]
            vphi = vec[1]
            vx   = vr*np.cos(phi) - vphi*r*np.sin(phi)
            vy   = vr*np.sin(phi) + vphi*r*np.cos(phi)
            return vx, vy
        elif CarpetHDF52D.plane == "xz":
            r      = coord[0]
            theta  = coord[1] + pi/4
            vr     = vec[0]
            vtheta = vec[1]
            vx     = vr*np.sin(theta) + vtheta*r*np.cos(theta)
            vz     = vr*np.cos(theta) - vtheta*r*np.sin(theta)
            return vx, vz
        elif CarpetHDF52D.plane == "yz":
            return vec

""" Scalar field 2D data """
class ScalarData2D(object):
    def __init__(self, field):
        assert isinstance(field, ScalarField)
        self.filenames = field.filenames
        self.field     = field
        self.label     = field.label

    def get_reflevel_data(self, rlidx, itern):
        """
        Get the data on the wanted reflevel as a masked array

        * rlidx : refinement level index
        * itern : iteration number
        """
        if rlidx not in list(CarpetHDF52D.dataset.select_reflevels(
                iteration=itern)):
            return None

        grid = CarpetHDF52D.get_reflevel(rlidx, itern)

        rdata    = np.empty(grid.n)
        rdata[:] = np.NAN
        for comp in grid.components:
            flist = []
            for var in self.field.fieldnames:
                flist.append(CarpetHDF52D.dataset.get_component_data(var,
                    itern, 0, comp.id[0], comp.id[1]))
            comp.restrict(rdata)[:] = self.field.compute(flist)

        return np.ma.masked_invalid(rdata)

    def get_grid_data(self, itern):
        """
        Get the data on the whole grid

        * itern : iteration number
        """
        out = [None for i in range(CarpetHDF52D.rlevels[-1] + 1)]
        for r in CarpetHDF52D.rlevels:
            out[r] = self.get_reflevel_data(r, itern)
        return out

    def init(self):
        pass

""" A field with value equal to the reflevel index  """
class ReflevelIndex2D(ScalarData2D):
    def __init__(self, field):
        super(ReflevelIndex2D, self).__init__(field)

    def get_reflevel_data(self, rlidx, itern):
        """
        Get the data on the wanted reflevel as a masked array

        * rlidx : refinement level index
        * itern : iteration number
        """
        if rlidx not in list(CarpetHDF52D.dataset.select_reflevels(
                iteration=itern)):
            return None

        grid = CarpetHDF52D.get_reflevel(rlidx, itern)

        rdata    = np.empty(grid.n)
        rdata[:] = float(rlidx)

        return np.ma.masked_invalid(rdata)

"""
This computes | grad u | / u, where u is a ScalarData2D
"""
class ContrastScalarData2D(ScalarData2D):
    def __init__(self, field):
        super(ContrastScalarData2D, self).__init__(field)

    def get_reflevel_data(self, rlidx, itern):
        """
        Get the data on the wanted reflevel as a masked array

        * rlidx  : refinement level index
        * itern  : iteration number
        """
        if rlidx not in list(CarpetHDF52D.dataset.select_reflevels(
                iteration=itern)):
            return None

        grid = CarpetHDF52D.get_reflevel(rlidx, itern)

        rdata    = np.empty(grid.n)
        rdata[:] = np.NAN
        for comp in grid.components:
            flist = []
            for var in self.field.fieldnames:
                flist.append(CarpetHDF52D.dataset.get_component_data(var,
                    itern, 0, comp.id[0], comp.id[1]))
            comp.restrict(rdata)[:] = self.field.compute(flist)

        rdata_x         = np.empty(rdata.shape)
        rdata_x[:]      = np.NAN
        rdata_x[1:-1,:] = (rdata[2:,:] - rdata[:-2,:])/grid.delta[0]

        rdata_y         = np.empty(rdata.shape)
        rdata_y[:]      = np.NAN
        rdata_y[:, 1:-1] = (rdata[:, 2:] - rdata[:, :-2])/grid.delta[1]

        out = np.sqrt(rdata_x**2 + rdata_y**2)/rdata
        return np.ma.masked_invalid(out)

"""
Variation of a 2D scalar field

This should be used only with static grids
"""
class DeltaScalarData2D(ScalarData2D):
    def __init__(self, field):
        super(DeltaScalarData2D, self).__init__(field)

    def get_reflevel_data(self, rlidx, itern):
        """
        Get the data on the wanted reflevel as a masked array

        * rlidx  : refinement level index
        * itern  : iteration number
        """
        f = super(DeltaScalarData2D, self).get_reflevel_data(rlidx, itern)
        return f - self.reference[rlidx]

    def init(self):
        """
        Store the reference value for the field
        """
        self.reference = [None for i in range(CarpetHDF52D.rlevels[-1] + 1)]
        it = CarpetHDF52D.dataset.iterations[0]
        for r in CarpetHDF52D.dataset.select_reflevels(iteration=it):
            self.reference[r] = super(DeltaScalarData2D,
                    self).get_reflevel_data(r, it)

""" Vector field 2D data """
class VectorData2D(object):
    def __init__(self, vector):
        assert isinstance(vector, VectorField)
        self.filenames = vector.filenames
        self.vector    = vector
        self.label     = vector.label

    def get_reflevel_data(self, rlidx, itern):
        """
        Get the data on the wanted reflevel as a tuple of masked arrays

        * rlidx : refinement level index
        * itern : iteration number
        """
        if not rlidx in list(CarpetHDF52D.dataset.select_reflevels(
                iteration=itern)):
            return (None, None)

        vidx = {
            "xy" : (0, 1),
            "xz" : (0, 2),
            "yz" : (1, 2)
            }[CarpetHDF52D.plane]

        grid = CarpetHDF52D.get_reflevel(rlidx, itern)

        vx    = np.empty(grid.n)
        vx[:] = np.NAN
        vy    = np.empty(grid.n)
        vy[:] = np.NAN

        for comp in grid.components:
            flist = []
            for var in self.vector.fieldnames:
                flist.append(CarpetHDF52D.dataset.get_component_data(var,
                    itern, 0, comp.id[0], comp.id[1]))
            vec = self.vector.compute(flist)
            comp.restrict(vx)[:] = vec[vidx[0]]
            comp.restrict(vy)[:] = vec[vidx[1]]

        if scivis.options['vector.average'] > 0:
            vx = signal.convolve(vx, scidata.kernels.square(2,
                    scivis.options['vector.average']), mode='same')
            vy = signal.convolve(vy, scidata.kernels.square(2,
                    scivis.options['vector.average']), mode='same')

            mask = np.logical_and(np.isfinite(vx), np.isfinite(vy))

            sl = (slice(0, vx.shape[0], 2*scivis.options['vector.average']),
                  slice(0, vx.shape[1], 2*scivis.options['vector.average']))

            mask[sl] = np.logical_not(mask[sl])
        else:
            mask = np.logical_or(np.logical_not(np.isfinite(vx)),
                    np.logical_not(np.isfinite(vy)))

        return (np.ma.masked_array(vx, mask=mask),
                np.ma.masked_array(vy, mask=mask))

    def get_grid_data(self, itern):
        """
        Get the data on the grid as a list of tuples of masked arrays

        * itern : iteration number
        """
        out = [None for i in range(CarpetHDF52D.rlevels[-1] + 1)]
        for r in CarpetHDF52D.rlevels:
            out[r] = self.get_reflevel_data(r, itern)
        return out

    def init(self):
        pass

""" Radial component of a vector """
class RadialVector2D(VectorData2D):
    def __init__(self, vector):
        super(RadialVector2D, self).__init__(vector)

    def get_reflevel_data(self, rlidx, itern):
        """
        Get the data on the wanted reflevel as a masked array

        * rlidx  : refinement level index
        * itern  : iteration number
        """
        if rlidx not in list(CarpetHDF52D.dataset.select_reflevels(
                iteration=itern)):
            return None

        vx, vy = super(RadialVector2D, self).get_reflevel_data(rlidx, itern)

        grid = CarpetHDF52D.get_reflevel(rlidx, itern)
        x, y = grid.mesh()
        r = np.sqrt(x**2 + y**2)

        return np.ma.masked_invalid((vx*x + vy*y)/r)
