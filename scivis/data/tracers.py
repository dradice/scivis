import abc
from bisect import bisect_left
import h5py
from math import pi, sin
import numpy as np
import scidata.gizmo as gizmo

import scivis
import scivis.plot
import scivis.units as units

from scivis.fields import ScalarField
from math import pi, sqrt
from scidata.utils import locate

""" Static Tracers array database """
class TracersDB(object):
    """
    * dataset : scidata.gizmo dataset
    """
    dataset = None

    @staticmethod
    def clean():
        TracersDB.dataset = None
    @staticmethod
    def init(root = '.'):
        flist = locate("tracers*hdf5", root=root, followlinks=True)
        TracersDB.dataset = gizmo.dataset(flist)

""" Tracers locations """
class TracerCoordinates(object):
    def __init__(self):
        if scivis.options["base.units"] == "cactus":
            unit = r"M_\odot"
        else:
            unit = r"{\rm km}"

        self.label = {}
        for plane in ["xy", "xz", "yz"]:
            self.label[plane] = {}
            for i, axis in enumerate(["x", "y"]):
                lbl = scivis.options["plot.label.{0}.{1}".format(plane, axis)]
                if lbl is None:
                    self.label[plane][axis] = r"${}\ [{}]$".format(
                            plane[i], unit)
                else:
                    self.label[plane][axis] = lbl.replace("@coord@", plane[i])

    def get_label(self, plane, axis):
        """
        Get the axis labels

        * plane : "xy", "xz" or "yz"
        * axis  : 0 or 1
        """
        return self.label[plane][axis]
    def get_labels(self, plane):
        """
        Get the axis labels

        * plane : "xy", "xz" or "yz"
        """
        return self.label[plane]

    def get_coords(self, itern):
        """
        Get the coordinates at the given iteration

        If the iteration does not exist, this performs a time interpolation
        from the neighbouring iterations

        * itern : iteration number
        """
        idx = bisect_left(TracersDB.dataset.iterations, itern)
        if(idx == 0):
            return TracersDB.dataset.get_coordinates(
                    TracersDB.dataset.iterations[0])
        if(idx == len(TracersDB.dataset.iterations)):
            return TracersDB.dataset.get_coordinates(
                    TracersDB.dataset.iterations[-1])

        it_0 = TracersDB.dataset.iterations[idx-1]
        it_1 = TracersDB.dataset.iterations[idx]
        theta = float(itern - it_0)/float(it_1 - it_0)
        #print it_0, itern, it_1

        coord_0 = TracersDB.dataset.get_coordinates(it=it_0, unpack=False)
        coord_1 = TracersDB.dataset.get_coordinates(it=it_1, unpack=False)

        coord = theta * coord_1 + (1 - theta) * coord_0

        return coord[:, 0], coord[:, 1], coord[:, 2]

    def double_copy_array(self, var):
        """
        Duplicates an array
        """
        siz = var.shape[0]
        out = np.empty(2*siz, dtype=var.dtype)
        out[0:siz] = var
        out[siz:]  = var
        return out
    def double_mirror_array(self, var):
        """
        Duplicate an array and change sign to the second half of the new array
        """
        siz = var.shape[0]
        out = np.empty(2*siz, dtype=var.dtype)
        out[0:siz] = var
        out[siz:]  = -var
        return out

    def sort_coordinates(self, plane, coord):
        """
        Re-order the coordinates in a way which depends on the current plot

        * coord : tuple of coordinates
        * plane : "xy", "xz" or "yz"
        """
        assert(plane in ["xy", "xz", "yz"])
        if plane == 'xy':
            x, y, z = coord[0], coord[1], coord[2]
        elif plane == 'xz':
            x, y, z = coord[0], coord[2], coord[1]
        elif plane == 'yz':
            x, y, z = coord[1], coord[2], coord[0]
        return x, y, z
    def make_slice(self, plane, coord):
        x, y, z = self.sort_coordinates(plane, coord)
        phi = scivis.options['plot.tracers.angle']*pi/180.0
        return np.abs(z) < np.sqrt(x*x + y*y)*sin(phi)

    def transf_coord(self, plane, coord):
        """
        Apply symmetries / coordinate transformations

        * coord : tuple of coordinates
        * plane : "xy", "xz" or "yz"
        """
        x, y, z = self.sort_coordinates(plane, coord)
        idx = self.make_slice(plane, coord)
        x = x[idx]
        y = y[idx]

        if scivis.options['base.units'] == 'metric':
            f = units.conv_length(units.cactus, units.metric, 1.0)/1000.0
        else:
            f = 1.0

        if plane == "xy":
            if 'rotating_180' in scivis.options['base.symmetries']:
                x = self.double_mirror_array(x)
                y = self.double_mirror_array(y)
        elif plane == "xz":
            if 'rotating_180' in scivis.options['base.symmetries']:
                x = self.double_mirror_array(x)
                y = self.double_copy_array(y)
                if 'reflecting_xy' in scivis.options['base.symmetries']:
                    x = self.double_copy_array(x)
                    y = self.double_mirror_array(y)
            elif 'reflecting_xy' in scivis.options['base.symmetries']:
                x = self.double_copy_array(x)
                y = self.double_mirror_array(y)
        elif plane == "yz":
            if 'reflecting_xy' in scivis.options['base.symmetries']:
                x = self.double_copy_array(x)
                y = self.double_mirror_array(y)

        return f*x, f*y

