import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.rN

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.vis.vector import VectorColorMap2D
from scivis.data.carpet2d import ScalarData2D, VectorData2D
from scivis.fields.thcm1 import rN

for ig in scivis.config.options['rN.groups']:
    ColorMap2D(ScalarData2D(rN(ig)), "rN[%d]" % ig).run()


# vim: set ft=python :
