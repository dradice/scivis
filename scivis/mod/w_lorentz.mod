import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.w_lorentz

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.hydro import LorentzFactor, LorentzFactorM1

if scivis.config.options['w_lorentz.minus_1']:
    ColorMap2D(ScalarData2D(LorentzFactorM1()), "w_lorentz_m_1").run()
else:
    ColorMap2D(ScalarData2D(LorentzFactor()), "w_lorentz").run()

# vim: set ft=python :
