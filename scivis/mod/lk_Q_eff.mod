import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.lk_Q_eff

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.leakage import QEffNue, QEffNua, QEffNux

ColorMap2D(ScalarData2D(QEffNue()), "Q_eff_nue").run()
ColorMap2D(ScalarData2D(QEffNua()), "Q_eff_nua").run()
ColorMap2D(ScalarData2D(QEffNux()), "Q_eff_nux").run()

# vim: set ft=python :
