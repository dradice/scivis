import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.vel_over_nu

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.hydro import VelocityOverShear

ColorMap2D(ScalarData2D(VelocityOverShear()), "vel_over_nu").run()

# vim: set ft=python :
