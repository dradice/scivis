import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.ham

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.spacetime import HamiltonianML, HamiltonianCTG

if scivis.config.options['ham.ctgamma']:
    ColorMap2D(ScalarData2D(HamiltonianCTG()), "ham").run()
else:
    ColorMap2D(ScalarData2D(HamiltonianML()), "ham").run()

# vim: set ft=python :
