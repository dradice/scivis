import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.velcomp
from config.velcomp import components

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.hydro import VelocityX, VelocityY, VelocityZ

assert len(components) > 0

if "x" in components:
    ColorMap2D(ScalarData2D(VelocityX()), "velx").run()
if "y" in components:
    ColorMap2D(ScalarData2D(VelocityY()), "vely").run()
if "z" in components:
    ColorMap2D(ScalarData2D(VelocityZ()), "velz").run()

# vim: set ft=python :
