import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.M0_E_nu

import scivis.config
scivis.config.validate()

from scivis.vis.array2d import VisualizeArray2D
from scivis.data.carpetar import ScalarDataAR
from scivis.fields.leakage import ENue, ENua, ENux

VisualizeArray2D(ScalarDataAR(ENue()), "M0_E_nue").run()
VisualizeArray2D(ScalarDataAR(ENua()), "M0_E_nua").run()
VisualizeArray2D(ScalarDataAR(ENux()), "M0_E_nux").run()

# vim: set ft=python :
