import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.vel_turb

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.hydro import TurbulentVelocity

ColorMap2D(ScalarData2D(TurbulentVelocity()), "vel_turb").run()

# vim: set ft=python :
