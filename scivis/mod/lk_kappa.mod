import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.lk_kappa

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.leakage import Kappa0Nue, Kappa0Nua, Kappa0Nux, \
                                  Kappa1Nue, Kappa1Nua, Kappa1Nux

ColorMap2D(ScalarData2D(Kappa0Nue()), "kappa_0_nue").run()
ColorMap2D(ScalarData2D(Kappa0Nua()), "kappa_0_nua").run()
ColorMap2D(ScalarData2D(Kappa0Nux()), "kappa_0_nux").run()
ColorMap2D(ScalarData2D(Kappa1Nue()), "kappa_1_nue").run()
ColorMap2D(ScalarData2D(Kappa1Nua()), "kappa_1_nua").run()
ColorMap2D(ScalarData2D(Kappa1Nux()), "kappa_1_nux").run()

# vim: set ft=python :
