from scivis.__version__ import __version__

# Import abstract module interface
from scivis.mod.module import *

# Import all the modules
from scivis.mod.grid import *
from scivis.mod.hydro import *
from scivis.mod.leakage import *
from scivis.mod.spacetime import *
from scivis.mod.thcm1 import *
from scivis.mod.zelmanim1 import *
