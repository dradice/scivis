import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.M0_N_nu

import scivis.config
scivis.config.validate()

from scivis.vis.array2d import VisualizeArray2D
from scivis.data.carpetar import ScalarDataAR
from scivis.fields.leakage import NNue, NNua, NNux

VisualizeArray2D(ScalarDataAR(NNue()), "M0_N_nue").run()
VisualizeArray2D(ScalarDataAR(NNua()), "M0_N_nua").run()
VisualizeArray2D(ScalarDataAR(NNux()), "M0_N_nux").run()

# vim: set ft=python :
