import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.lk_abs_energy

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.leakage import AbsEnergy

ColorMap2D(ScalarData2D(AbsEnergy()), "lk_abs_energy").run()

# vim: set ft=python :
