import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.lk_optd

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.leakage import Optd0Nue, Optd0Nua, Optd0Nux, \
                                  Optd1Nue, Optd1Nua, Optd1Nux

ColorMap2D(ScalarData2D(Optd0Nue()), "optd_0_nue").run()
ColorMap2D(ScalarData2D(Optd0Nua()), "optd_0_nua").run()
ColorMap2D(ScalarData2D(Optd0Nux()), "optd_0_nux").run()
ColorMap2D(ScalarData2D(Optd1Nue()), "optd_1_nue").run()
ColorMap2D(ScalarData2D(Optd1Nua()), "optd_1_nua").run()
ColorMap2D(ScalarData2D(Optd1Nux()), "optd_1_nux").run()

# vim: set ft=python :
