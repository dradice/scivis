import abc, six

""" All the modules """
modlist = {}

""" Modules dictionary """
moddict = {}

@six.add_metaclass(abc.ABCMeta)
class Abstract(object):
    """
    Abstract interface for a module class
    """

    name   = ""
    desc   = ""
    script = ""
    config = ""
    tags   = []

def register(mod):
    """
    Register a module
    """
    assert isinstance(mod, Abstract)

    modlist[mod.name] = mod

    if "all" in moddict:
        moddict["all"].add(mod)
    else:
        moddict["all"] = {mod}

    for t in mod.tags:
        if t in moddict:
            moddict[t].add(mod)
        else:
            moddict[t] = {mod}
