import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.lk_luminosity

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.leakage import LuminosityNue, LuminosityNua, LuminosityNux

ColorMap2D(ScalarData2D(LuminosityNue()), "luminosity_nue").run()
ColorMap2D(ScalarData2D(LuminosityNua()), "luminosity_nua").run()
ColorMap2D(ScalarData2D(LuminosityNux()), "luminosity_nux").run()

# vim: set ft=python :
