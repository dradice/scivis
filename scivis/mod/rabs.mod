import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.rabs

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.vis.vector import VectorColorMap2D
from scivis.data.carpet2d import ScalarData2D, VectorData2D
from scivis.fields.thcm1 import rAbs

for it in (0,1):
    for ig in scivis.config.options['rabs.groups']:
        ColorMap2D(ScalarData2D(rAbs(it, ig)), "rabs_%d[%d]" % (it, ig)).run()

# vim: set ft=python :
