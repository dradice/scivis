import scivis.mod.module as module

class Grid(module.Abstract):
    name    = "grid"
    desc    = "Plots the grid structure."
    script  = "grid"
    config  = "grid"
    tags    = []
module.register(Grid())
