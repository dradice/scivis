import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.M0_abs_nu

import scivis.config
scivis.config.validate()

from scivis.vis.array2d import VisualizeArray2D
from scivis.data.carpetar import ScalarDataAR
from scivis.fields.leakage import AbsNue, AbsNua

VisualizeArray2D(ScalarDataAR(AbsNue()), "M0_abs_nue").run()
VisualizeArray2D(ScalarDataAR(AbsNua()), "M0_abs_nua").run()

# vim: set ft=python :
