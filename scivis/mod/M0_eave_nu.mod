import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.M0_eave_nu

import scivis.config
scivis.config.validate()

from scivis.vis.array2d import VisualizeArray2D
from scivis.data.carpetar import ScalarDataAR
from scivis.fields.leakage import EaveNue, EaveNua, EaveNux

VisualizeArray2D(ScalarDataAR(EaveNue()), "M0_eave_nue").run()
VisualizeArray2D(ScalarDataAR(EaveNua()), "M0_eave_nua").run()
VisualizeArray2D(ScalarDataAR(EaveNux()), "M0_eave_nux").run()

# vim: set ft=python :
