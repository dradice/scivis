import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.rE

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.vis.vector import VectorColorMap2D
from scivis.data.carpet2d import ScalarData2D, VectorData2D
from scivis.fields.thcm1 import rE, rFoE, rF

for ig in scivis.config.options['rE.groups']:
    if scivis.config.options['rE.flux']:
        if scivis.config.options['rE.flux.normalize']:
            VectorColorMap2D(
                    ScalarData2D(rE(ig)),
                    VectorData2D(rFoE(ig)),
                    "rE[%d]" % ig
            ).run()
        else:
            VectorColorMap2D(
                    ScalarData2D(rE(ig)),
                    VectorData2D(rF(ig)),
                    "rE[%d]" % ig
            ).run()
    else:
        ColorMap2D(ScalarData2D(rE(ig)), "rE[%d]" % ig).run()


# vim: set ft=python :
