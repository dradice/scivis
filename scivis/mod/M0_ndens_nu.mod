import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.M0_ndens_nu

import scivis.config
scivis.config.validate()

from scivis.vis.array2d import VisualizeArray2D
from scivis.data.carpetar import ScalarDataAR
from scivis.fields.leakage import NdensNue, NdensNua, NdensNux

VisualizeArray2D(ScalarDataAR(NdensNue()), "M0_ndens_nue").run()
VisualizeArray2D(ScalarDataAR(NdensNua()), "M0_ndens_nua").run()
VisualizeArray2D(ScalarDataAR(NdensNux()), "M0_ndens_nux").run()

# vim: set ft=python :
