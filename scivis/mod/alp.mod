import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.alp

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.vis.vector import VectorColorMap2D
from scivis.data.carpet2d import ScalarData2D, VectorData2D
from scivis.fields.spacetime import Lapse, ShiftVector

if scivis.config.options['alp.shiftvector']:
    VectorColorMap2D(
            ScalarData2D(Lapse()),
            VectorData2D(ShiftVector()),
            "alp"
    ).run()
else:
    ColorMap2D(ScalarData2D(Lapse()), "alp").run()

# vim: set ft=python :
