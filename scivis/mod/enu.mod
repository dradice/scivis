import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.enu

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.vis.vector import VectorColorMap2D
from scivis.data.carpet2d import ScalarData2D, VectorData2D
from scivis.fields.zelmanim1 import ENu, FoENu

if scivis.config.options['enu.flux']:
    VectorColorMap2D(
            ScalarData2D(ENu(scivis.config.options["enu.group"])),
            VectorData2D(FoENu(scivis.config.options["enu.group"])),
            "enu"
    ).run()
else:
    ColorMap2D(ScalarData2D(ENu(scivis.config.options["enu.group"])),
            "enu").run()


# vim: set ft=python :
