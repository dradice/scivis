import scivis.mod.module as module

class M1Enu(module.Abstract):
    name   = "enu"
    desc   = "Plots the neutrino energy density, optionally the flux"
    script = "enu"
    config = "enu"
    tags   = ["ZM1", "radiation"]
module.register(M1Enu())

