import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.grid

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ReflevelIndex2D 
from scivis.fields.hydro import Density

ColorMap2D(ReflevelIndex2D(Density()), "grid").run()

# vim: set ft=python :
