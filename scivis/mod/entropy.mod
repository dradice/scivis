import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.entropy

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.vis.vector import VectorColorMap2D
from scivis.data.carpet2d import ScalarData2D, VectorData2D
from scivis.fields.hydro import Entropy, SpecEntropyFlux

if scivis.config.options['entropy.plotflux']:
    VectorColorMap2D(
            ScalarData2D(Entropy()),
            VectorData2D(SpecEntropyFlux()),
            "entropy"
    ).run()
else:
    ColorMap2D(ScalarData2D(Entropy()), "entropy").run()

# vim: set ft=python :
