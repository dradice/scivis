import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.M0_grid

import scivis.config
scivis.config.validate()

from scivis.vis.array2d import VisualizeArray2D
from scivis.data.carpetar import ScalarDataAR
from scivis.fields.leakage import THCLkX, THCLkY, THCLkZ

VisualizeArray2D(ScalarDataAR(THCLkX()), "M0_x").run()
VisualizeArray2D(ScalarDataAR(THCLkY()), "M0_y").run()
VisualizeArray2D(ScalarDataAR(THCLkZ()), "M0_z").run()

# vim: set ft=python :
