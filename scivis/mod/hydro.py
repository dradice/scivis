import scivis.mod.module as module

class Color(module.Abstract):
    name    = "color"
    desc    = "Plots the color tracer."
    script  = "color"
    config  = "color"
    tags    = ["hydro"]
module.register(Color())

class Dens(module.Abstract):
    name    = "dens"
    desc    = "Plots the conservative density."
    script  = "dens"
    config  = "dens"
    tags    = ["hydro"]
module.register(Dens())

class DensGain(module.Abstract):
    name    = "densgain"
    desc    = "Plots the gain/loss of rest mass density."
    script  = "densgain"
    config  = "densgain"
    tags    = ["hydro"]
module.register(DensGain())

class DensUnbnd(module.Abstract):
    name    = "dens_unbnd"
    desc    = "Plots the unbound density"
    script  = "dens_unbnd"
    config  = "dens_unbnd"
    tags    = ["hydro"]
module.register(DensUnbnd())

class DeltaRho(module.Abstract):
    name    = "drho"
    desc    = "Plots the variation of the density. Optionally the mass flux."
    script  = "drho"
    config  = "drho"
    tags    = ["hydro"]
module.register(DeltaRho())

class DLogP(module.Abstract):
    name    = "dlogp"
    desc    = "Plots the pressure contrast."
    script  = "dlogp"
    config  = "dlogp"
    tags    = ["hydro"]
module.register(DLogP())

class Entropy(module.Abstract):
    name    = "entropy"
    desc    = "Plots the entropy per baryon."
    script  = "entropy"
    config  = "entropy"
    tags    = ["hydro"]
module.register(Entropy())

class Enthalpy(module.Abstract):
    name    = "enthalpy"
    desc    = "Plots the enthalpy."
    script  = "enthalpy"
    config  = "enthalpy"
    tags    = ["hydro"]
module.register(Enthalpy())

class Epsilon(module.Abstract):
    name    = "eps"
    desc    = "Plots the specific internal energy."
    script  = "eps"
    config  = "eps"
    tags    = ["hydro"]
module.register(Epsilon())

class ElectronFraction(module.Abstract):
    name    = "Y_e"
    desc    = "Plots the electron fraction."
    script  = "Y_e"
    config  = "Y_e"
    tags    = ["hydro"]
module.register(ElectronFraction())

class ElectronDegeneracy(module.Abstract):
    name    = "eta_e"
    desc    = "Plots the electron degeneracy parameter."
    script  = "eta_e"
    config  = "eta_e"
    tags    = ["hydro"]
module.register(ElectronDegeneracy())

class EnergyInf(module.Abstract):
    name    = "eninf"
    desc    = "Plots the specific energy at infinity, ie - u_t - 1."
    script  = "eninf"
    config  = "eninf"
    tags    = ["hydro"]
module.register(EnergyInf())

class LightbulbNetHeating(module.Abstract):
    name    = "lb_net_heat"
    desc    = "Plots the net heating from the lightbulb scheme."
    script  = "lb_net_heat"
    config  = "lb_net_heat"
    tags    = ["hydro", "radiation"]
module.register(LightbulbNetHeating())

class HyperonMassFraction(module.Abstract):
    name    = "XL"
    desc    = "Plots the Lambda hyperon mass fraction."
    script  = "XL"
    config  = "XL"
    tags    = ["hydro"]
module.register(HyperonMassFraction())

class OmegaBV(module.Abstract):
    name    = "omegabv"
    desc    = "Plots the Brunt-Vaisala frequency."
    script  = "omegabv"
    config  = "omegabv"
    tags    = ["hydro", "ccsn"]
module.register(OmegaBV())

class PolytropicConstant(module.Abstract):
    name    = "polyconst"
    desc    = "Plots the effective polytropic constant."
    script  = "polyconst"
    config  = "polyconst"
    tags    = ["hydro"]
module.register(PolytropicConstant())

class Rho(module.Abstract):
    name    = "rho"
    desc    = "Plots the density. Optionally the mass flux."
    script  = "rho"
    config  = "rho"
    tags    = ["hydro"]
module.register(Rho())

class RhoXn(module.Abstract):
    name    = "rhoxn"
    desc    = "Plots the neutron density."
    script  = "rhoxn"
    config  = "rhoxn"
    tags    = ["hydro"]
module.register(RhoXn())

class RhoXp(module.Abstract):
    name    = "rhoxp"
    desc    = "Plots the proton density."
    script  = "rhoxp"
    config  = "rhoxp"
    tags    = ["hydro"]
module.register(RhoXp())

class RhoCGS(module.Abstract):
    name    = "rho_cgs"
    desc    = "Plots the density in CGS."
    script  = "rho_cgs"
    config  = "rho_cgs"
    tags    = ["hydro"]
module.register(RhoCGS())

class Tau(module.Abstract):
    name    = "tau"
    desc    = "Plots the conservative tau."
    script  = "tau"
    config  = "tau"
    tags    = ["hydro"]
module.register(Tau())

class Temperature(module.Abstract):
    name    = "temperature"
    desc    = "Plots the temperature in MeV."
    script  = "temperature"
    config  = "temperature"
    tags    = ["hydro"]
module.register(Temperature())

class TurbulentVelocity(module.Abstract):
    name    = "vel_turb"
    desc    = "Plots the turbulent radial velocity."
    script  = "vel_turb"
    config  = "vel_turb"
    tags    = ["hydro", "ccsn"]
module.register(TurbulentVelocity())

class VelocityOverShear(module.Abstract):
    name    = "vel_over_nu"
    desc    = "Plots the ratio of velocity to shear viscosity."
    script  = "vel_over_nu"
    config  = "vel_over_nu"
    tags    = ["hydro"]
module.register(VelocityOverShear())

class VelComp(module.Abstract):
    name    = "velcomp"
    desc    = "Plots one or more components of the velocity."
    script  = "velcomp"
    config  = "velcomp"
    tags    = ["hydro"]
module.register(VelComp())

class VelNorm(module.Abstract):
    name    = "vel"
    desc    = "Plots the magnitude of the velocity."
    script  = "vel"
    config  = "vel"
    tags    = ["hydro"]
module.register(VelNorm())

class VelRad(module.Abstract):
    name    = "vel_rad"
    desc    = "Plots the radial component of the velocity."
    script  = "vel_rad"
    config  = "vel_rad"
    tags    = ["hydro"]
module.register(VelRad())

class VorticityXY(module.Abstract):
    name    = "vorticity_xy"
    desc    = "Plots the xy component of the vorticity tensor."
    script  = "vorticity_xy"
    config  = "vorticity_xy"
    tags    = ["hydro"]
module.register(VorticityXY())

class WLorentz(module.Abstract):
    name    = "w_lorentz"
    desc    = "Plots the Lorentz factor."
    script  = "w_lorentz"
    config  = "w_lorentz"
    tags    = ["hydro"]
module.register(WLorentz())
