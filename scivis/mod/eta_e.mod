import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.eta_e

import scivis.config
scivis.config.validate()

import scivis.eostable as eos
eos.init(scivis.options["eos.table"])

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.hydro import ElectronDegeneracy

ColorMap2D(ScalarData2D(ElectronDegeneracy()), "eta_e").run()

# vim: set ft=python :
