import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.rho

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.vis.vector import VectorColorMap2D
from scivis.data.carpet2d import ScalarData2D, VectorData2D
from scivis.fields.hydro import Density, MassFlux

if scivis.config.options['rho.massflux']:
    VectorColorMap2D(
            ScalarData2D(Density()),
            VectorData2D(MassFlux()),
            "rho"
    ).run()
else:
    ColorMap2D(ScalarData2D(Density()), "rho").run()


# vim: set ft=python :
