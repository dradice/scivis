import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.vel_rad

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import RadialVector2D
from scivis.fields.hydro import Velocity

ColorMap2D(RadialVector2D(Velocity()), "vel_rad").run()

# vim: set ft=python :
