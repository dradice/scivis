import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.drho

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.vis.vector import VectorColorMap2D
from scivis.data.carpet2d import DeltaScalarData2D, VectorData2D
from scivis.fields.hydro import Density, MassFlux

if scivis.config.options['drho.massflux']:
    VectorColorMap2D(
            DeltaScalarData2D(Density()),
            VectorData2D(MassFlux()),
            "drho"
    ).run()
else:
    ColorMap2D(DeltaScalarData2D(Density()), "drho").run()

# vim: set ft=python :
