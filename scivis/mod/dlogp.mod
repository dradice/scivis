import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.dlogp

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ContrastScalarData2D
from scivis.fields.hydro import Pressure

ColorMap2D(ContrastScalarData2D(Pressure()), "dlogp").run()

# vim: set ft=python :
