import sys
import os
if all(os.getcwd() != s for s in sys.path):
    sys.path = [os.getcwd()] + sys.path

import config.base
import config.lk_R_eff

import scivis.config
scivis.config.validate()

from scivis.vis.field import ColorMap2D
from scivis.data.carpet2d import ScalarData2D
from scivis.fields.leakage import REffNue, REffNua, REffNux

ColorMap2D(ScalarData2D(REffNue()), "R_eff_nue").run()
ColorMap2D(ScalarData2D(REffNua()), "R_eff_nua").run()
ColorMap2D(ScalarData2D(REffNux()), "R_eff_nux").run()

# vim: set ft=python :
