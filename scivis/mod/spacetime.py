import scivis.mod.module as module

class Alp(module.Abstract):
    name   = "alp"
    desc   = "Plots the lapse function. Optionally the shift vector."
    script = "alp"
    config = "alp"
    tags   = ["spacetime"]
module.register(Alp())

class Ham(module.Abstract):
    name   = "ham"
    desc   = "Plots the Hamiltonian constraint."
    script = "ham"
    config = "ham"
    tags   = ["constraints"]
module.register(Ham())

class VolForm(module.Abstract):
    name   = "volform"
    desc   = "Plots the spatial volume form."
    script = "volform"
    config = "volform"
    tags   = ["spacetime"]
module.register(VolForm())

