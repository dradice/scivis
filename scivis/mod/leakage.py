import scivis.mod.module as module

##############################################################################
# Fields defined on the Carpet grid
##############################################################################
class LKAbsEnergy(module.Abstract):
    name   = "lk_abs_energy"
    desc   = "Plots the energy absorption rate of free-streaming neutrinos"
    script = "lk_abs_energy"
    config = "lk_abs_energy"
    tags   = ["leakage"]
module.register(LKAbsEnergy())

class LKAbsNumber(module.Abstract):
    name   = "lk_abs_number"
    desc   = "Plots the number absorption rate of free-streaming neutrinos"
    script = "lk_abs_number"
    config = "lk_abs_number"
    tags   = ["leakage"]
module.register(LKAbsNumber())

class LKKappa(module.Abstract):
    name   = "lk_kappa"
    desc   = "Plots the neutrino opacities"
    script = "lk_kappa"
    config = "lk_kappa"
    tags   = ["leakage"]
module.register(LKKappa())

class LKREff(module.Abstract):
    name   = "lk_R_eff"
    desc   = "Plots the effective number emission rate"
    script = "lk_R_eff"
    config = "lk_R_eff"
    tags   = ["leakage"]
module.register(LKREff())

class LKQEff(module.Abstract):
    name   = "lk_Q_eff"
    desc   = "Plots the effective energy emission rate"
    script = "lk_Q_eff"
    config = "lk_Q_eff"
    tags   = ["leakage"]
module.register(LKQEff())

class LKLuminosity(module.Abstract):
    name   = "lk_luminosity"
    desc   = "Plots the effective leakage luminosity"
    script = "lk_luminosity"
    config = "lk_luminosity"
    tags   = ["leakage"]
module.register(LKLuminosity())

class LKOptd(module.Abstract):
    name   = "lk_optd"
    desc   = "Plots the neutrino optical depths"
    script = "lk_optd"
    config = "lk_optd"
    tags   = ["leakage"]
module.register(LKOptd())

##############################################################################
# Fields defined on the M0 grid
##############################################################################
class M0Abs(module.Abstract):
    name   = "M0_abs_nu"
    desc   = "Plots the neutrino absorption opacities"
    script = "M0_abs_nu"
    config = "M0_abs_nu"
    tags   = ["M0"]
module.register(M0Abs())

class M0EaveNu(module.Abstract):
    name   = "M0_eave_nu"
    desc   = "Plots the average energy of the free-streaming neutrinos"
    script = "M0_eave_nu"
    config = "M0_eave_nu"
    tags   = ["M0"]
module.register(M0EaveNu())

class M0ENu(module.Abstract):
    name   = "M0_E_nu"
    desc   = "Plots the evolved avg energy of the free-streaming neutrinos"
    script = "M0_E_nu"
    config = "M0_E_nu"
    tags   = ["M0"]
module.register(M0ENu())

class M0Grid(module.Abstract):
    name   = "M0_grid"
    desc   = "Plots the leakage M0 grid"
    script = "M0_grid"
    config = "M0_grid"
    tags   = ["debug"]
module.register(M0Grid())

class M0NdensNu(module.Abstract):
    name   = "M0_ndens_nu"
    desc   = "Plots the free-streaming neutrino density"
    script = "M0_ndens_nu"
    config = "M0_ndens_nu"
    tags   = ["M0"]
module.register(M0NdensNu())

class M0NNu(module.Abstract):
    name   = "M0_N_nu"
    desc   = "Plots the free-streaming neutrino conserved density"
    script = "M0_N_nu"
    config = "M0_N_nu"
    tags   = ["M0"]
module.register(M0NNu())

