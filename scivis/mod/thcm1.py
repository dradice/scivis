import scivis.mod.module as module

class M1Nnu(module.Abstract):
    name   = "rN"
    desc   = "Plots the neutrino number density"
    script = "rN"
    config = "rN"
    tags   = ["M1", "radiation"]
module.register(M1Nnu())

class M1Abs(module.Abstract):
    name   = "rabs"
    desc   = "Plots the absorption opacities"
    script = "rabs"
    config = "rabs"
    tags   = ["M1", "radiation"]
module.register(M1Abs())

class M1NetAbs(module.Abstract):
    name   = "rnetabs"
    desc   = "Plots the net absorption rate"
    script = "rnetabs"
    config = "rnetabs"
    tags   = ["M1", "radiation"]
module.register(M1NetAbs())

class M1NetHeatCGS(module.Abstract):
    name   = "rnetheat_cgs"
    desc   = "Plots the net heating rage (CGS)"
    script = "rnetheat_cgs"
    config = "rnetheat_cgs"
    tags   = ["M1", "radiation"]
module.register(M1NetHeatCGS())

class M1Ynu(module.Abstract):
    name   = "rYnu"
    desc   = "Plots the neutrino number fraction"
    script = "rYnu"
    config = "rYnu"
    tags   = ["M1", "radiation"]
module.register(M1Ynu())

class M1Enu(module.Abstract):
    name   = "rE"
    desc   = "Plots the neutrino energy density, optionally the flux"
    script = "rE"
    config = "rE"
    tags   = ["M1", "radiation"]
module.register(M1Enu())

class M1DensEqNu(module.Abstract):
    name   = "rdens_eq_nu"
    desc   = "Plots the equilibrium number density of neutrinos"
    script = "rdens_eq_nu"
    config = "rdens_eq_nu"
    tags   = ["M1", "radiation"]
module.register(M1DensEqNu())

class M1Eave(module.Abstract):
    name   = "reave"
    desc   = "Plots the average neutrino energy in MeV"
    script = "reave"
    config = "reave"
    tags   = ["M1", "radiation"]
module.register(M1Eave())
