import abc
import sys

import numpy as np
import scivis.eostable as eos
import scivis.units as units
from scivis.fields import ScalarField, VectorField

##############################################################################
# Scalar fields
##############################################################################
class Color0(ScalarField):
    name       = "color[0]"
    label      = "$c$"
    filenames  = ["color[0]"]
    fieldnames = ["THC_COLOR::color[0]"]

    def compute(self, fields):
        return fields[0]

class ConsDensity(ScalarField):
    name       = "dens"
    label      = "$D$"
    filenames  = ["rho", "w_lorentz"]
    fieldnames = ["HYDROBASE::rho", "HYDROBASE::w_lorentz"]

    def compute(self, fields):
        return fields[0]*fields[1]

class ConsDensityUnbound(ScalarField):
    name       = "dens_unbnd"
    label      = r"$D_{\rm unbound}$"
    filenames  = ["dens_unbnd"]
    fieldnames = ['BNSANALYSIS::dens_unbnd']

    def compute(self, fields):
        return fields[0]

class ConsTau(ScalarField):
    name       = "tau"
    label      = r"$\tau$"
    filenames  = ["rho", "w_lorentz", "eps", "press"]
    fieldnames = ["HYDROBASE::rho", "HYDROBASE::w_lorentz", "HYDROBASE::eps",
            "HYDROBASE::press"]

    def compute(self, fields):
        rho       = fields[0]
        w_lorentz = fields[1]
        eps       = fields[2]
        press     = fields[3]

        dens  = rho*w_lorentz
        W2    = w_lorentz**2
        rho_h = rho*(1 + eps) + press

        return rho_h*W2 - press - dens

class Density(ScalarField):
    name       = "rho"
    label      = r"$\rho$"
    filenames  = ["rho"]
    fieldnames = ["HYDROBASE::rho"]

    def compute(self, fields):
        return fields[0]

class DensityGain(ScalarField):
    name       = "densgain"
    label      = "densgain"
    filenames  = ["densgain"]
    fieldnames = ["THC_CORE::densgain"]

    def compute(self, fields):
        return fields[0]

class DensityNeutrons(ScalarField):
    name       = "rhoxn"
    label      = r"$\rho_n$"
    filenames  = ["rho", "Y_e"]
    fieldnames = ["HYDROBASE::rho", "HYDROBASE::Y_e"]

    def compute(self, fields):
        return fields[0] * (1 - fields[1])

class DensityProtons(ScalarField):
    name       = "rhoxp"
    label      = r"$\rho_p$"
    filenames  = ["rho", "Y_e"]
    fieldnames = ["HYDROBASE::rho", "HYDROBASE::Y_e"]

    def compute(self, fields):
        return fields[0] * fields[1]

class DensityCGS(ScalarField):
    name       = 'rho_cgs'
    label      = r"$\rho\ [{\rm g}\ {\rm cm}^{-3}]$"
    filenames  = ["rho"]
    fieldnames = ["HYDROBASE::rho"]

    def compute(self, fields):
        return units.conv_dens(units.cactus, units.cgs, fields[0])

class ElectronFraction(ScalarField):
    name       = "Y_e"
    label      = r"$Y_e$"
    filenames  = ["rho", "Y_e"]
    fieldnames = ['HYDROBASE::Y_e']

    def compute(self, fields):
        return fields[0]

class ElectronDegeneracy(ScalarField):
    name       = "eta_e"
    label      = r"$\eta_e$"
    filenames  = ["rho", "temperature", "Y_e"]
    fieldnames = ["HYDROBASE::rho", "HYDROBASE::temperature", "HYDROBASE::Y_e"]

    def compute(self, fields):
        mu_e = eos.evaluate("mu_e", fields[0], fields[1], fields[2])
        return mu_e/fields[2]

class EnergyInf(ScalarField):
    name       = "eninf"
    label      = r"$-u_t - 1$"
    filenames  = ["eninf"]
    fieldnames = ["THC_ANALYSIS::eninf"]

    def compute(self, fields):
        return fields[0]

class Entropy(ScalarField):
    name       = "entropy"
    label      = r"$S$"
    filenames  = ["entropy"]
    fieldnames = ["HYDROBASE::entropy"]

    def compute(self, fields):
        return fields[0]

class Enthalpy(ScalarField):
    name       = "enthalpy"
    label      = r"$H$"
    filenames  = ["rho", "eps", "press"]
    fieldnames = ['HYDROBASE::rho', 'HYDROBASE::eps', 'HYDROBASE::press']

    def compute(self, fields):
        return 1.0 + fields[1] + fields[2]/fields[0]

class Epsilon(ScalarField):
    name       = "eps"
    label      = r"$\epsilon$"
    filenames  = ["eps"]
    fieldnames = ['HYDROBASE::eps']

    def compute(self, fields):
        return fields[0]

class HyperonMassFraction(ScalarField):
    name       = "hyper"
    label      = r"$X_\Lambda$"
    filenames  = ["rho", "temperature", "Y_e"]
    fieldnames = ["HYDROBASE::rho", "HYDROBASE::temperature", "HYDROBASE::Y_e"]

    def compute(self, fields):
        return eos.evaluate("XL", fields[0], fields[1], fields[2]) + 1.0e-16

class LightbulbNetHeating(ScalarField):
    name       = "lb_net_heat"
    label      = r"$\dot{Q}_{\rm net}$"
    filenames  = ["lightbulb_net_heating"]
    fieldnames = ['THC_SIMPLECCSN::lightbulb_net_heating']

    def compute(self, fields):
        return fields[0]

class LorentzFactor(ScalarField):
    name       = "w_lorentz"
    label      = r"$W$"
    filenames  = ["w_lorentz"]
    fieldnames = ['HYDROBASE::w_lorentz']

    def compute(self, fields):
        return fields[0]

class LorentzFactorM1(LorentzFactor):
    name       = "w_lorentz_m_1"
    label      = r"$W-1$"

    def compute(self, fields):
        return super(LorentzFactorM1, self).compute(fields) - 1

class OmegaBV(ScalarField):
    name       = "OmegaBV"
    label      = r"$\Omega_{\rm BV}$"
    filenames  = ["OmegaBV"]
    fieldnames = ["THC_SIMPLECCSN::OmegaBV"]

    def compute(self, fields):
        return fields[0]

class Pressure(ScalarField):
    name       = "press"
    label      = r"$p$"
    filenames  = ["press"]
    fieldnames = ['HYDROBASE::press']

    def compute(self, fields):
        return fields[0]

class PolytropicConstant(ScalarField):
    name       = "K"
    label      = r"$K$"
    filenames  = ['rho', 'press']
    fieldnames = ['HYDROBASE::rho', 'HYDROBASE::press']

    adiabindex = 2.0

    def compute(self, fields):
        rho   = fields[0]
        press = fields[1]
        K     = press/(rho**PolytropicConstant.adiabindex)

        return K

class Temperature(ScalarField):
    name       = "temperature"
    label      = r"$T\ [{\rm MeV}]$"
    filenames  = ["temperature"]
    fieldnames = ['HYDROBASE::temperature']

    def compute(self, fields):
        return fields[0]

class TurbulentVelocity(ScalarField):
    name       = "vel_turb"
    label      = r"vel_turb"
    filenames  = ["vel_turb"]
    fieldnames = ["THC_SIMPLECCSN::vel_turb"]

    def compute(self, fields):
        return fields[0]

class VelocityOverShear(ScalarField):
    name       = "vel_over_nu"
    label      = r"$v/\nu\ [{\rm km^{-1}}]$"
    filenames  = ["w_lorentz", "rho", "temperature"]
    fieldnames = [
            "HYDROBASE::w_lorentz",
            "HYDROBASE::rho",
            "HYDROBASE::temperature"
    ]

    def compute(self, fields):
        W   = fields[0]
        rho = units.conv_dens(units.cactus, units.cgs, fields[1])
        T   = fields[2]

        # Estimate velocity
        vel = np.sqrt(1.0 - 1.0/(W*W))
        # Convert to km s^-1
        vel = units.conv_velocity(units.cactus, units.cgs, vel)/1e5

        # Estimate shear viscosity, in units of km^2 s^-1
        # See 1410.1874, Eq. (10)
        nu  = 1.2 * (T/10)**2 * (rho/1e13)**(-2)

        return vel/nu

class VelocityNorm(ScalarField):
    name       = "vel"
    label      = r"$v$"
    filenames  = ["w_lorentz"]
    fieldnames = ["HYDROBASE::w_lorentz"]

    def compute(self, fields):
        return np.sqrt(1.0 - 1.0/(fields[0]**2))

class VelocityX(ScalarField):
    name       = "velx"
    label      = r"$v^x$"
    filenames  = ['vel[0]']
    fieldnames = ['HYDROBASE::vel[0]']

    def compute(self, fields):
        return fields[0]

class VelocityY(ScalarField):
    name       = "vely"
    label      = r"$v^y$"
    filenames  = ['vel[1]']
    fieldnames = ['HYDROBASE::vel[1]']

    def compute(self, fields):
        return fields[0]

class VelocityZ(ScalarField):
    name       = "velz"
    label      = r"$v^z$"
    filenames  = ['vel[2]']
    fieldnames = ['HYDROBASE::vel[2]']

    def compute(self, fields):
        return fields[0]

class VorticityXY(ScalarField):
    name       = "vorticity_xy"
    label      = r"$\omega_{xy}$"
    filenames  = ['vorticity_xy']
    fieldnames = ['THC_ANALYSIS::vorticity_xy']

    def compute(self, fields):
        return fields[0]

##############################################################################
# Vector fields
##############################################################################
class MassFlux(VectorField):
    name        = "mflux"
    label       = r"$\rho W (\alpha \vec{v} - \vec{\beta})$"
    filenames   = ['vel[0]', 'vel[1]', 'vel[2]', 'rho', 'w_lorentz',
            'alp', 'betax', 'betay', 'betaz']
    fieldnames  = ['HYDROBASE::vel[0]', 'HYDROBASE::vel[1]',
            'HYDROBASE::vel[2]', 'HYDROBASE::rho', 'HYDROBASE::w_lorentz',
            'ADMBASE::alp', 'ADMBASE::betax', 'ADMBASE::betay',
            'ADMBASE::betaz']

    def compute(self, fields):
        velx  = fields[0]
        vely  = fields[1]
        velz  = fields[2]
        rho   = fields[3]
        W     = fields[4]
        alp   = fields[5]
        betax = fields[6]
        betay = fields[7]
        betaz = fields[8]

        return rho*W*(alp*velx - betax),\
               rho*W*(alp*vely - betay),\
               rho*W*(alp*velz - betaz)

class SpecEntropyFlux(VectorField):
    name        = "sflux"
    label       = r"$S \vec{v}$"
    filenames   = ["vel[0]", "vel[1]", "vel[2]", "entropy"]
    fieldnames  = ['HYDROBASE::vel[0]', 'HYDROBASE::vel[1]',
            'HYDROBASE::vel[2]', 'HYDROBASE::entropy']
    def compute(self, fields):
        velx  = fields[0]
        vely  = fields[1]
        velz  = fields[2]

        entr      = np.zeros_like(fields[3])
        idx       = fields[3] > 0
        entr[idx] = fields[3][idx]

        return entr*velx, entr*vely, entr*velz

class Velocity(VectorField):
    name        = "vel"
    label       = r"$\vec{v}$"
    filenames   = ['vel[0]', 'vel[1]', 'vel[2]']
    fieldnames  = ['HYDROBASE::vel[0]', 'HYDROBASE::vel[1]',
            'HYDROBASE::vel[2]']

    def compute(self, fields):
        return fields[0], fields[1], fields[2]
