import abc
import sys

import numpy as np
import scivis.units as ut
from scivis.fields import ScalarField, VectorField

class rN(ScalarField):
    name_p       = "rN[%d]"
    label_p      = "rN[%d]"
    filenames_p  = ["rN[%d]"]
    fieldnames_p = ["THC_M1::rN[%d]"]

    def __init__(self, n):
        self.name       = rN.name_p % n
        self.label      = rN.label_p % n
        self.filenames  = [p % n for p in rN.filenames_p]
        self.fieldnames = [p % n for p in rN.fieldnames_p]

    def compute(self, fields):
        return fields[0]

class rYnu(ScalarField):
    name_p       = "ynu%s"
    label_p      = "ynu%s"
    filenames_p  = ["ynu%s"]
    fieldnames_p = ["THC_M1::ynu%s"]

    def __init__(self, s):
        assert s in ["e", "a", "x"]
        self.name       = rYnu.name_p % s
        self.label      = rYnu.label_p % s
        self.filenames  = [p % s for p in rYnu.filenames_p]
        self.fieldnames = [p % s for p in rYnu.fieldnames_p]

    def compute(self, fields):
        return fields[0]

class rAbs(ScalarField):
    name_p       = "abs_%d[%d]"
    label_p      = "abs_%d[%d]"
    filenames_p  = ["abs_%d[%d]"]
    fieldnames_p = ["THC_M1::abs_%d[%d]"]

    def __init__(self, t, n):
        self.name       = rAbs.name_p % (t, n)
        self.label      = rAbs.label_p % (t, n)
        self.filenames  = [rAbs.filenames_p[0] % (t, n)]
        self.fieldnames = [rAbs.fieldnames_p[0] % (t, n)]

    def compute(self, fields):
        return fields[0]

class rNetAbs(ScalarField):
    name       = "netabs"
    label      = "netabs"
    filenames  = ["netabs"]
    fieldnames = ["THC_M1::netabs"]

    def compute(self, fields):
        return fields[0]

class rNetHeatCGS(ScalarField):
    name       = "netheat"
    label      = "netheat"
    filenames  = ["netheat"]
    fieldnames = ["THC_M1::netheat"]

    def compute(self, fields):
        u_e = ut.conv_energy_density(ut.cactus, ut.cgs, 1.0)
        u_t = ut.conv_time(ut.cactus, ut.cgs, 1.0)
        return fields[0]*(u_e/u_t)   # erg s^-1 cm^-3

class rE(ScalarField):
    name_p       = "rE[%d]"
    label_p      = "rE[%d]"
    filenames_p  = ["rE[%d]"]
    fieldnames_p = ["THC_M1::rE[%d]"]

    def __init__(self, n):
        self.name       = rE.name_p % n
        self.label      = rE.label_p % n
        self.filenames  = [p % n for p in rE.filenames_p]
        self.fieldnames = [p % n for p in rE.fieldnames_p]

    def compute(self, fields):
        return fields[0]

class rFoE(VectorField):
    name_p       = "rf[%d]"
    label_p      = "rf[%d]"
    filenames_p  = ["rE[%d]", "rFx[%d]", "rFy[%d]", "rFz[%d]"]
    fieldnames_p = ["THC_M1::rE[%d]",  "THC_M1::rFx[%d]",
                    "THC_M1::rFy[%d]", "THC_M1::rFz[%d]"]

    def __init__(self, n):
        self.name       = rFoE.name_p % n
        self.label      = rFoE.label_p % n
        self.filenames  = [p % n for p in rFoE.filenames_p]
        self.fieldnames = [p % n for p in rFoE.fieldnames_p]

    def compute(self, fields):
        return fields[1]/fields[0], \
               fields[2]/fields[0], \
               fields[3]/fields[0]

class rF(VectorField):
    name_p       = "rF[%d]"
    label_p      = "rF[%d]"
    filenames_p  = ["rFx[%d]", "rFy[%d]", "rFz[%d]"]
    fieldnames_p = ["THC_M1::rFx[%d]", "THC_M1::rFy[%d]",
                    "THC_M1::rFz[%d]"]

    def __init__(self, n):
        self.name       = rF.name_p % n
        self.label      = rF.label_p % n
        self.filenames  = [p % n for p in rF.filenames_p]
        self.fieldnames = [p % n for p in rF.fieldnames_p]

    def compute(self, fields):
        return fields[0], fields[1], fields[2]

class rdens_eq_nu(ScalarField):
    name_p       = "rdens_eq_nu[%d]"
    label_p      = "rdens_eq_nu[%d]"
    filenames_p  = ["abs_0[%d]", "eta_0[%d]"]
    fieldnames_p = ["THC_M1::abs_0[%d]", "THC_M1::eta_0[%d]"]

    def __init__(self, s):
        n               = {"e": 0, "a": 1, "x": 2}[s]
        self.name       = rdens_eq_nu.name_p % n
        self.label      = rdens_eq_nu.label_p % n
        self.filenames  = [p % n for p in rdens_eq_nu.filenames_p]
        self.fieldnames = [p % n for p in rdens_eq_nu.fieldnames_p]

    def compute(self, fields):
        return fields[1]/fields[0]

class reave(ScalarField):
    name_p       = "reave[%d]"
    label_p      = "reave[%d]"
    filenames_p  = ["rN[%d]", "rE[%d]"]
    fieldnames_p = ["THC_M1::rN[%d]", "THC_M1::rE[%d]"]

    def __init__(self, s):
        n               = {"e": 0, "a": 1, "x": 2}[s]
        self.name       = reave.name_p % n
        self.label      = reave.label_p % n
        self.filenames  = [p % n for p in reave.filenames_p]
        self.fieldnames = [p % n for p in reave.fieldnames_p]

    def compute(self, fields):
        # This corresponds to WeakRates::normfact
        nfac = 1.0e+50
        # Compute the average energy in Cactus units
        eave = fields[1]/(nfac*fields[0])
        # Convert to erg
        eave = ut.conv_energy(ut.cactus, ut.cgs, eave)
        # Return in MeV
        return eave/(1e6*ut.cgs.eV)
