import abc

from scivis.fields import ScalarField, VectorField

##############################################################################
# Scalar fields
##############################################################################
class Lapse(ScalarField):
    name       = "alp"
    label      = r"$\alpha$"
    filenames  = ["alp"]
    fieldnames = ["ADMBASE::alp"]

    def compute(self, fields):
        return fields[0]

class HamiltonianML(ScalarField):
    name       = "ham"
    label      = r"$H$"
    filenames  = ["H"]
    fieldnames = ["ML_ADMCONSTRAINTS::H"]

    def compute(self, fields):
        return fields[0]

class HamiltonianCTG(ScalarField):
    name       = "ham"
    label      = r"$H$"
    filenames  = ["H"]
    fieldnames = ["CTGCONSTRAINTS::H"]

    def compute(self, fields):
        return fields[0]

class VolForm(ScalarField):
    name       = "volform"
    label      = r"Vol"
    filenames  = ["volform"]
    fieldnames = ["THC_CORE::volform"]

    def compute(self, fields):
        return fields[0]

##############################################################################
# Vector fields
##############################################################################
class ShiftVector(VectorField):
    name       = "beta"
    label      = r"$\vec{\beta}$"
    filenames  = ["betax", "betay", "betaz"]
    fieldnames = ["ADMBASE::betax", "ADMBASE::betay", "ADMBASE::betaz"]

    def compute(self, fields):
        return fields[0], fields[1], fields[2]

class Momentum(VectorField):
    name       = "mom"
    label      = r"$\vec{M}$"
    filenames  = ["M1", "M2", "M3"]
    fieldnames = ["ML_ADMCONSTRAINTS::M1", "ML_ADMCONSTRAINTS::M2",
            "ML_ADMCONSTRAINTS::M3"]

    def compute(self, fields):
        return fields[0], fields[1], fields[2]
