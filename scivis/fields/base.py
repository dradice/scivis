import abc, six

""" An abstract scalar field """
@six.add_metaclass(abc.ABCMeta)
class ScalarField(object):
    """Field name"""
    name       = ""

    """Label"""
    label      = ""

    """Basename of the files containing the data"""
    filenames  = []

    """Cactus variables required to compute the field"""
    fieldnames = []

    @abc.abstractmethod
    def compute(self, fields):
        """Computes the field"""
        pass

""" An abstract scalar field """
class VectorField(object):
    """Field name"""
    name       = ""

    """Label"""
    label      = ""

    """Basename of the files containing the data"""
    filenames  = []

    """Cactus variables required to compute the field"""
    fieldnames = []

    @abc.abstractmethod
    def compute(self, fields):
        """Computes the field"""
        pass

