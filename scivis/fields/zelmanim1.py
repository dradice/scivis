import abc
import sys

import numpy as np
import scivis.units as ut
from scivis.fields import ScalarField, VectorField

class ENu(ScalarField):
    name_p       = "enu[%d]"
    label_p      = "enu[%d]"
    filenames_p  = ["enu[%d]"]
    fieldnames_p = ["ZELMANIM1::enu[%d]"]

    def __init__(self, n):
        self.name       = ENu.name_p % n
        self.label      = ENu.label_p % n
        self.filenames  = [p % n for p in ENu.filenames_p]
        self.fieldnames = [p % n for p in ENu.fieldnames_p]

    def compute(self, fields):
        return fields[0]

class FoENu(VectorField):
    name_p       = "fnu[%d]"
    label_p      = "fnu[%d]"
    filenames_p  = ["enu[%d]", "fnux[%d]", "fnuy[%d]", "fnuz[%d]"]
    fieldnames_p = ["ZELMANIM1::enu[%d]", "ZELMANIM1::fnux[%d]",
                    "ZELMANIM1::fnuy[%d]", "ZELMANIM1::fnuz[%d]"]

    def __init__(self, n):
        self.name       = FoENu.name_p % n
        self.label      = FoENu.label_p % n
        self.filenames  = [p % n for p in FoENu.filenames_p]
        self.fieldnames = [p % n for p in FoENu.fieldnames_p]

    def compute(self, fields):
        return fields[1]/fields[0], fields[2]/fields[0], \
               fields[3]/fields[0]

class FNu(VectorField):
    name_p       = "fnu[%d]"
    label_p      = "fnu[%d]"
    filenames_p  = ["fnux[%d]", "fnuy[%d]", "fnuz[%d]"]
    fieldnames_p = ["ZELMANIM1::fnux[%d]", "ZELMANIM1::fnuy[%d]",
                    "ZELMANIM1::fnuz[%d]"]

    def __init__(self, n):
        self.name       = FNu.name_p % n
        self.label      = Fnu.label_p % n
        self.filenames  = [p % n for p in FNu.filenames_p]
        self.fieldnames = [p % n for p in FNu.fieldnames_p]

    def compute(self, fields):
        return fields[0], fields[1], fields[2]
