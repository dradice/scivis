import abc
import sys

import numpy as np
import scivis.units as ut
from scivis.fields import ScalarField

##############################################################################
# Fields defined on the Carpet grid
##############################################################################
class Kappa0Nue(ScalarField):
    name	= "kappa_0_nue"
    label       = r"$\kappa_{\nu_e, 0}$"
    filenames   = ["kappa_0_nue"]
    fieldnames  = ["THC_LEAKAGEBASE::kappa_0_nue"]
    def compute(self, fields):
        return fields[0]
class Kappa0Nua(ScalarField):
    name	= "kappa_0_nua"
    label       = r"$\kappa_{\nu_a, 0}$"
    filenames   = ["kappa_0_nua"]
    fieldnames  = ["THC_LEAKAGEBASE::kappa_0_nua"]
    def compute(self, fields):
        return fields[0]
class Kappa0Nux(ScalarField):
    name	= "kappa_0_nux"
    label       = r"$\kappa_{\nu_x, 0}$"
    filenames   = ["kappa_0_nux"]
    fieldnames  = ["THC_LEAKAGEBASE::kappa_0_nux"]
    def compute(self, fields):
        return fields[0]
class Kappa1Nue(ScalarField):
    name	= "kappa_1_nue"
    label       = r"$\kappa_{\nu_e, 1}$"
    filenames   = ["kappa_1_nue"]
    fieldnames  = ["THC_LEAKAGEBASE::kappa_1_nue"]
    def compute(self, fields):
        return fields[0]
class Kappa1Nua(ScalarField):
    name	= "kappa_1_nua"
    label       = r"$\kappa_{\nu_a, 1}$"
    filenames   = ["kappa_1_nua"]
    fieldnames  = ["THC_LEAKAGEBASE::kappa_1_nua"]
    def compute(self, fields):
        return fields[0]
class Kappa1Nux(ScalarField):
    name	= "kappa_1_nux"
    label       = r"$\kappa_{\nu_x, 1}$"
    filenames   = ["kappa_1_nux"]
    fieldnames  = ["THC_LEAKAGEBASE::kappa_1_nux"]
    def compute(self, fields):
        return fields[0]

class Optd0Nue(ScalarField):
    name	= "optd_0_nue"
    label       = r"$\tau_{\nu_e, 0}$"
    filenames   = ["optd_0_nue"]
    fieldnames  = ["THC_LEAKAGEBASE::optd_0_nue"]
    def compute(self, fields):
        return fields[0]
class Optd0Nua(ScalarField):
    name	= "optd_0_nua"
    label       = r"$\tau_{\nu_a, 0}$"
    filenames   = ["optd_0_nua"]
    fieldnames  = ["THC_LEAKAGEBASE::optd_0_nua"]
    def compute(self, fields):
        return fields[0]
class Optd0Nux(ScalarField):
    name	= "optd_0_nux"
    label       = r"$\tau_{\nu_x, 0}$"
    filenames   = ["optd_0_nux"]
    fieldnames  = ["THC_LEAKAGEBASE::optd_0_nux"]
    def compute(self, fields):
        return fields[0]
class Optd1Nue(ScalarField):
    name	= "optd_1_nue"
    label       = r"$\tau_{\nu_e, 1}$"
    filenames   = ["optd_1_nue"]
    fieldnames  = ["THC_LEAKAGEBASE::optd_1_nue"]
    def compute(self, fields):
        return fields[0]
class Optd1Nua(ScalarField):
    name	= "optd_1_nua"
    label       = r"$\tau_{\nu_e, 1}$"
    filenames   = ["optd_1_nua"]
    fieldnames  = ["THC_LEAKAGEBASE::optd_1_nua"]
    def compute(self, fields):
        return fields[0]
class Optd1Nux(ScalarField):
    name	= "optd_1_nux"
    label       = r"$\tau_{\nu_e, 1}$"
    filenames   = ["optd_1_nux"]
    fieldnames  = ["THC_LEAKAGEBASE::optd_1_nux"]
    def compute(self, fields):
        return fields[0]

class REffNue(ScalarField):
    name        = "R_eff_nue"
    label       = r"$R^{\rm eff}_{\nu_e}$"
    filenames   = ["R_eff_nue"]
    fieldnames  = ["THC_LEAKAGEBASE::R_eff_nue"]
    def compute(self, fields):
        return fields[0]
class REffNua(ScalarField):
    name        = "R_eff_nua"
    label       = r"$R^{\rm eff}_{\nu_a}$"
    filenames   = ["R_eff_nua"]
    fieldnames  = ["THC_LEAKAGEBASE::R_eff_nua"]
    def compute(self, fields):
        return fields[0]
class REffNux(ScalarField):
    name        = "R_eff_nux"
    label       = r"$R^{\rm eff}_{\nu_x}$"
    filenames   = ["R_eff_nux"]
    fieldnames  = ["THC_LEAKAGEBASE::R_eff_nux"]
    def compute(self, fields):
        return fields[0]

class QEffNue(ScalarField):
    name        = "Q_eff_nue"
    label       = r"$Q^{\rm eff}_{\nu_e}$"
    filenames   = ["Q_eff_nue"]
    fieldnames  = ["THC_LEAKAGEBASE::Q_eff_nue"]
    def compute(self, fields):
        return fields[0]
class QEffNua(ScalarField):
    name        = "Q_eff_nua"
    label       = r"$Q^{\rm eff}_{\nu_a}$"
    filenames   = ["Q_eff_nua"]
    fieldnames  = ["THC_LEAKAGEBASE::Q_eff_nua"]
    def compute(self, fields):
        return fields[0]
class QEffNux(ScalarField):
    name        = "Q_eff_nux"
    label       = r"$Q^{\rm eff}_{\nu_x}$"
    filenames   = ["Q_eff_nux"]
    fieldnames  = ["THC_LEAKAGEBASE::Q_eff_nux"]
    def compute(self, fields):
        return fields[0]

# In units of 10^53 ergs/s
class LuminosityNue(ScalarField):
    name        = "luminosity_nue"
    label       = r"$L_{\nu_e}\ [10^{53}\ {\rm erg}\ {\rm s}^{-1}]$"
    filenames   = ["luminosity_nue"]
    fieldnames  = ["THC_LEAKAGEBASE::luminosity_nue"]
    def compute(self, fields):
        factor = ut.conv_luminosity(ut.cactus,
                                    ut.cgs, 1.0)/1.0e53 # 3628504.9849130646
        return fields[0]*factor
# In units of 10^53 ergs/s
class LuminosityNua(ScalarField):
    name        = "luminosity_nua"
    label       = r"$L_{\nu_a}\ [10^{53}\ {\rm erg}\ {\rm s}^{-1}]$"
    filenames   = ["luminosity_nua"]
    fieldnames  = ["THC_LEAKAGEBASE::luminosity_nua"]
    def compute(self, fields):
        factor = ut.conv_luminosity(ut.cactus,
                                    ut.cgs, 1.0)/1.0e53 # 3628504.9849130646
        return fields[0]*factor
# In units of 10^53 ergs/s
class LuminosityNux(ScalarField):
    name        = "luminosity_nux"
    label       = r"$L_{\nu_x}\ [10^{53}\ {\rm erg}\ {\rm s}^{-1}]$"
    filenames   = ["luminosity_nux"]
    fieldnames  = ["THC_LEAKAGEBASE::luminosity_nux"]
    def compute(self, fields):
        factor = ut.conv_luminosity(ut.cactus,
                                    ut.cgs, 1.0)/1.0e53 # 3628504.9849130646
        return fields[0]*factor

class AbsNumber(ScalarField):
    name        = "abs_number"
    label       = r"$\dot{N}_\nu$"
    filenames   = ["abs_number"]
    fieldnames  = ["THC_LEAKAGEBASE::abs_number"]
    def compute(self, fields):
        return fields[0]
class AbsEnergy(ScalarField):
    name        = "abs_energy"
    label       = r"$\dot{Q}_\nu$"
    filenames   = ["abs_energy"]
    fieldnames  = ["THC_LEAKAGEBASE::abs_energy"]
    def compute(self, fields):
        return fields[0]

##############################################################################
# Fields defined on the M0 grid
##############################################################################
class THCLkX(ScalarField):
    name        = "thc_M0_x"
    label       = r"$x$"
    filenames   = ["thc_M0_x"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_x"]
    def compute(self, fields):
        return fields[0]
class THCLkY(ScalarField):
    name        = "thc_M0_y"
    label       = r"$y$"
    filenames   = ["thc_M0_y"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_y"]
    def compute(self, fields):
        return fields[0]
class THCLkZ(ScalarField):
    name        = "thc_M0_z"
    label       = r"$z$"
    filenames   = ["thc_M0_z"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_z"]
    def compute(self, fields):
        return fields[0]

class AbsNue(ScalarField):
    name        = "thc_M0_abs_nue"
    label       = r"$\dot{N}_{\nu_e}$"
    filenames   = ["thc_M0_abs_nue"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_abs_nue"]
    def compute(self, fields):
        return fields[0]
class AbsNua(ScalarField):
    name        = "thc_M0_abs_nua"
    label       = r"$\dot{N}_{\nu_a}$"
    filenames   = ["thc_M0_abs_nua"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_abs_nua"]
    def compute(self, fields):
        return fields[0]

class NdensNue(ScalarField):
    name	= "thc_M0_ndens_nue"
    label       = r"$N_{\nu_e}$"
    filenames   = ["thc_M0_ndens_nue"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_ndens_nue"]
    def compute(self, fields):
        return fields[0]
class NdensNua(ScalarField):
    name	= "thc_M0_ndens_nua"
    label       = r"$N_{\nu_a}$"
    filenames   = ["thc_M0_ndens_nua"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_ndens_nua"]
    def compute(self, fields):
        return fields[0]
class NdensNux(ScalarField):
    name	= "thc_M0_ndens_nux"
    label       = r"$N_{\nu_x}$"
    filenames   = ["thc_M0_ndens_nux"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_ndens_nux"]
    def compute(self, fields):
        return fields[0]

class NNue(ScalarField):
    name        = "thc_M0_N_nue"
    label       = r"$n_{\nu_e}$"
    filenames   = ["thc_M0_N_nue"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_N_nue"]
    def compute(self, fields):
        return fields[0]
class NNua(ScalarField):
    name        = "thc_M0_N_nua"
    label       = r"$n_{\nu_a}$"
    filenames   = ["thc_M0_N_nua"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_N_nua"]
    def compute(self, fields):
        return fields[0]
class NNux(ScalarField):
    name        = "thc_M0_N_nux"
    label       = r"$n_{\nu_x}$"
    filenames   = ["thc_M0_N_nux"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_N_nux"]
    def compute(self, fields):
        return fields[0]

class EaveNue(ScalarField):
    name	= "thc_M0_eave_nue"
    label       = r"$E_{\nu_e}\ [{\rm MeV}]$"
    filenames   = ["thc_M0_eave_nue"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_eave_nue"]
    def compute(self, fields):
        normfac    = 1e50
        erg2mev    = 624150.934
        cactus2erg = ut.conv_energy(ut.cactus, ut.cgs, 1)
        fac        = cactus2erg * erg2mev / normfac # 11157023440.543983
        return fac * fields[0]
class EaveNua(ScalarField):
    name	= "thc_M0_eave_nua"
    label       = r"$E_{\nu_a}\ [{\rm MeV}]$"
    filenames   = ["thc_M0_eave_nua"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_eave_nua"]
    def compute(self, fields):
        normfac    = 1e50
        erg2mev    = 624150.934
        cactus2erg = ut.conv_energy(ut.cactus, ut.cgs, 1)
        fac        = cactus2erg * erg2mev / normfac # 11157023440.543983
        return fac * fields[0]
class EaveNux(ScalarField):
    name	= "thc_M0_eave_nux"
    label       = r"$E_{\nu_x}\ [{\rm MeV}]$"
    filenames   = ["thc_M0_eave_nux"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_eave_nux"]
    def compute(self, fields):
        normfac    = 1e50
        erg2mev    = 624150.934
        cactus2erg = ut.conv_energy(ut.cactus, ut.cgs, 1)
        fac        = cactus2erg * erg2mev / normfac # 11157023440.543983
        return fac * fields[0]

class ENue(ScalarField):
    name        = "thc_M0_E_nue"
    label       = r"$\mathcal{E}_{\nu_e}\ [{\rm MeV}]$"
    filenames   = ["thc_M0_E_nue"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_E_nue"]
    def compute(self, fields):
        return fields[0]
class ENua(ScalarField):
    name        = "thc_M0_E_nua"
    label       = r"$\mathcal{E}_{\nu_a}\ [{\rm MeV}]$"
    filenames   = ["thc_M0_E_nua"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_E_nua"]
    def compute(self, fields):
        return fields[0]
class ENux(ScalarField):
    name        = "thc_M0_E_nux"
    label       = r"$\mathcal{E}_{\nu_x}\ [{\rm MeV}]$"
    filenames   = ["thc_M0_E_nux"]
    fieldnames  = ["THC_LEAKAGEM0::thc_M0_E_nux"]
    def compute(self, fields):
        return fields[0]
