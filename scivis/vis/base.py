import abc, six
import pprint
import os

import scivis
import scivis.movie as movie
import scivis.plot as plot
import scivis.utils as utils

from numpy import inf
from scivis.data.carpet2d import *
from scivis.data.bhdiag import *
from scivis.data.tracers import *

""" Class implementing a 2D visualization """
@six.add_metaclass(abc.ABCMeta)
class Visualization2D(object):
    def __init__(self, data, rootpath='.'):
        self.data = data
        self.root = rootpath

        if scivis.options['base.coordinates'] == 'cartesian':
            self.coord = CartesianCoordinates2D()
        elif scivis.options['base.coordinates'] == 'spherical':
            self.coord = SphericalCoordinates2D()
        else:
            raise ValueError("Unknown coordinate type: " +
                    scivis.options['base.coordinates'])

        if scivis.options['plot.tracers']:
            TracersDB.clean()
            TracersDB.init(root=scivis.options['base.datapath'])
            self.tracers = TracerCoordinates()
        if scivis.options["plot.horizons"]:
            BHDiagnostics.clean()
            BHDiagnostics.init(root=scivis.options["base.datapath"])
            self.horizon = BHHorizon()

        if scivis.options['plot.scale'] == 'log':
            self.root = 'log_' + self.root
        elif scivis.options['plot.scale'] == 'log_abs':
            self.root = 'log_abs_' + self.root

        utils.mkdir("output/" + self.root)

        open("output/" + self.root + "/config.txt", "w").write(
                pprint.pformat(scivis.options) + "\n")

    def compute_scale(self, fdata):
        """
        Compute the scale of a given field dataset
        """
        scale = [inf, -inf]

        k = 0.0
        t = float(len(CarpetHDF52D.dataset.iterations))
        for it in CarpetHDF52D.dataset.iterations:
            utils.printer.progress(
                    "---> {0} {1} : computing field extrema... {2} %".format(
                        self.root, self.plane, int(k/t*100)))

            try:
                r = max(self.wanted_rlevel.intersection(set(
                    CarpetHDF52D.dataset.select_reflevels(iteration=it))))
                field = fdata.get_reflevel_data(r, it)
                scale[0] = min(scale[0], field.min())
                scale[1] = max(scale[1], field.max())
            # No data to plot for current iteration
            except ValueError:
                pass

            k += 1.0
        utils.printer.final(
                "---> {0} {1} : computing field extrema... done!".format(
                    self.root, self.plane))

        return tuple(scale)

    def compute_norm(self, vdata):
        """
        Computes the maximum norm of a given vector field dataset
        """
        norm = 0

        k = 0.0
        t = float(len(CarpetHDF52D.dataset.iterations))
        for it in CarpetHDF52D.dataset.iterations:
            utils.printer.progress(
                    "---> {0} {1} : computing vector norm... {2} %".format(
                        self.root, self.plane, int(k/t*100)))

            r = max(self.wanted_rlevel.intersection(set(
                CarpetHDF52D.dataset.select_reflevels(iteration=it))))
            coord = self.coord.get_coord(it)[r]
            vec = vdata.get_reflevel_data(r, it)
            vx, vy = self.coord.transf_vector(coord, vec)
            norm = max(norm, np.ma.masked_invalid(np.sqrt(
                vx*vx + vy*vy)).max())

            k += 1.0
        utils.printer.final(
                "---> {0} {1} : computing vector norm... done!".format(
                    self.root, self.plane))

        return norm

    def compute_nframes(self):
        """
        Compute the number of frames to plot
        """
        nframes = len(CarpetHDF52D.dataset.iterations)
        if scivis.options['base.reflevel'] == "merge":
            pass
        elif scivis.options['base.reflevel'] == "full" and\
                len(self.wanted_rlevel) > 1:
            nframes = nframes*(len(self.wanted_rlevel) + 1)
        elif scivis.options['base.reflevel'] == "given":
            nframes = nframes*len(self.wanted_rlevel)
        return nframes

    def make_movie(self):
        dirn = os.listdir(self.basepath)
        dirn = [d for d in dirn if os.path.isdir(self.basepath + "/" + d)]
        for d in dirn:
            utils.printer.progress("---> {0} {1} : {2}/movie.mp4".format(
                self.root, self.plane, self.basepath + "/" + d))
            movie.ffmpeg(self.basepath + "/" + d)
        utils.printer.final("---> {0} {1} : movies done!".format(
            self.root, self.plane))

    @abc.abstractmethod
    def plot_plane(self):
        pass

    def run(self):
        """
        Runs the visualization
        """
        for p in scivis.options['plot.planes']:
            self.set_plane(p)
            self.plot_plane()
            if scivis.options['base.makemovies']:
                self.make_movie()

    def set_plane(self, plane):
        """
        Set the current plane
        """
        assert plane in ["xy", "xz", "yz"]

        # Set the current plane
        self.plane = plane

        # Read the data
        CarpetHDF52D.clean()
        CarpetHDF52D.init(plane = plane,
                root = scivis.options['base.datapath'])
        for d in self.data:
            CarpetHDF52D.register(d)
        CarpetHDF52D.read()

        # Create directory structure
        utils.mkdir("output/{0}".format(self.root))
        self.basepath = "output/{0}/{1}".format(self.root, plane)
        utils.mkdir(self.basepath)

        self.wanted_rlevel = set(CarpetHDF52D.rlevels)
        if scivis.options['base.reflevel'] == 'given':
            self.wanted_rlevel = self.wanted_rlevel.intersection(
                    set(scivis.options['base.levels']))

        if scivis.options['base.reflevel'] != "merge":
            for l in self.wanted_rlevel:
                utils.mkdir(self.basepath + "/r{0}".format(l))
        if scivis.options['base.reflevel'] == "merge" or\
                (scivis.options['base.reflevel'] == "full" and
                        len(self.wanted_rlevel) > 1):
            utils.mkdir(self.basepath + "/rM")
