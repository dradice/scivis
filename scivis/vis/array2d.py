import pprint
import os

import scivis
import scivis.movie as movie
import scivis.plot as plot
import scivis.utils as utils

from numpy import inf
from scivis.data.carpetar import *

# TODO: parallelize this class too
class VisualizeArray2D(object):
    def __init__(self, data, rootpath='.'):
        self.data = data
        self.root = rootpath

        CarpetHDF5AR.clean()
        CarpetHDF5AR.init(scivis.options['base.datapath'])
        CarpetHDF5AR.register(self.data)
        CarpetHDF5AR.read()

        if scivis.options['array.coordinates'] == 'THC_Leakage':
            self.coord = THCLeakageCoordinates()
        else:
            raise ValueError("Unknown coordinate type: " +
                    scivis.options['array.coordinates'])

        if scivis.options['plot.scale'] == 'log':
            self.root = 'log_' + self.root
        elif scivis.options['plot.scale'] == 'log_abs':
            self.root = 'log_abs_' + self.root

        utils.mkdir("output/" + self.root)

        open("output/" + self.root + "/config.txt", "w").write(
                pprint.pformat(scivis.options) + "\n")

    def compute_scale(self, fdata):
        """
        Computes the scale of a given field dataset
        """
        assert isinstance(fdata, ScalarDataAR)

        scale = [inf, -inf]

        k = 0.0
        t = float(len(CarpetHDF5AR.dataset.iterations))
        for it in CarpetHDF5AR.dataset.iterations:
            utils.printer.progress(
                    "---> {0} {1} : computing field extrema... {2} %".format(
                        self.root, self.plane, int(k/t*100)))
            self.coord.read_grid(it)
            field = self.coord.transf_scalar(fdata.get_data(it))
            scale[0] = min(scale[0], field.min())
            scale[1] = max(scale[1], field.max())
            k += 1.0
        utils.printer.final(
                "---> {0} {1} : computing field extrema... done!".format(
                    self.root, self.plane))

    def compute_nframes(self):
        """
        Computes the number of frames to plot
        """
        return len(CarpetHDF5AR.dataset.iterations)

    def make_movie(self):
        utils.printer.progress("---> {0} {1} : {2}/movie.mp4".format(
            self.root, self.plane, self.basepath))
        movie.ffmpeg(self.basepath)
        utils.printer.final("---> {0} {1} : movies done!".format(
            self.root, self.plane))

    def plot_plane(self):
        itfill = 9

        fnum    = 0.0
        nframes = float(self.compute_nframes())

        # Compute the scale of the plot
        if isinstance(scivis.options['plot.range'], tuple):
            scale = scivis.options['plot.range']
        elif scivis.options['plot.range'] == 'auto':
            scale = self.compute_scale(self.data)
        else:
            scale = None

        for it in CarpetHDF5AR.dataset.iterations:
            # Read the data
            self.coord.read_grid(it)
            coord = self.coord.get_coord(it)
            cell  = self.coord.get_cells(it)
            var   = self.coord.transf_scalar(self.data.get_data(it))

            # Skip if all NaNs
            if np.alltrue(var.mask):
                fnum += 1.0
                continue

            # Find the current time and convert to ms if needed
            time  = CarpetHDF5AR.dataset.get_time(it)
            if scivis.options['base.units'] == 'metric':
                time  = units.conv_time(units.cactus, units.metric, time)*1000
                title = r'$t = %7.3f\ [\mathrm{ms}]$' % time
            else:
                title = r'$t = %10.2f$' % time

            # Do the actual plotting
            itstr = str(it).zfill(itfill)
            fname = self.basepath + "/" + itstr + ".png"
            if os.path.isfile(fname) and scivis.options['base.continue'] == True:
                utils.printer.progress('{} already present! Skip'.format(fname))
                continue
            utils.printer.progress("---> {0} {1} : {2} ({3} %)".format(
                self.root, self.plane, fname, int(fnum/nframes*100)))

            # Plot
            ax = plot.axes()
            im = plot.mycolorplot(ax, [coord], [cell], [var], scale)
            if im is None:
                fnum += 1.0
                continue
            plot.myaxis(ax, coord, scivis.options['base.axis'], self.plane)

            # Colorbar
            if scivis.options['plot.scale'] == 'linear':
                cb = plot.colorbar(im, format=plot.ScalarFormatter(),
                        ticks=scivis.options['plot.cbar.ticks'])
            else:
                cb = plot.colorbar(im,
                        format=plot.LogFormatterMathtext(),
                        ticks=scivis.options['plot.cbar.ticks'])
            if scivis.options['plot.label.cbar'] is not None:
                cb.set_label(scivis.options['plot.label.cbar'])

            # Labels
            ax.set_title(title)
            ax.set_xlabel(self.coord.get_label('x'))
            ax.set_ylabel(self.coord.get_label('y'))
            plot.savefig(fname)
            plot.close()

            fnum += 1.0
        utils.printer.final("---> {0} {1} : plots done!".format(self.root,
            self.plane))
    def run(self):
        """
        Runs the visualization
        """
        for p in scivis.options['plot.planes']:
            self.set_plane(p)
            self.plot_plane()
            if scivis.options['base.makemovies']:
                self.make_movie()

    def set_plane(self, plane):
        """
        Set the current plane
        """
        assert plane in ["xy", "xz", "yz"]

        # Set the current plane
        self.plane = plane
        self.coord.set_plane(plane)

        # Create directory structure
        utils.mkdir("output/{0}".format(self.root))
        self.basepath = "output/{0}/{1}".format(self.root, plane)
        utils.mkdir(self.basepath)
