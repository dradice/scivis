import abc
import numpy as np
from multiprocessing import Pool, Lock, Value
import types
import copyreg

from gc import collect

import scivis.plot as plot
import scivis
import scivis.units as units
import scivis.utils as utils

from numpy import inf
from scivis.data.carpet2d import CarpetHDF52D
from scivis.fields import ScalarField, VectorField
from scivis.vis import Visualization2D

# Global counter
# https://stackoverflow.com/a/2080668
counter = None

# hack to get multiprocessing to work with instance methods
# http://stackoverflow.com/questions/25156768
def _pickle_method(m):
    if m.__self__ is None:
        return getattr, (m.__self__.__class__, m.__func__.__name__)
    else:
        return getattr, (m.__self__, m.__func__.__name__)

copyreg.pickle(types.MethodType, _pickle_method)

""" Class implementing an array visualization on top of a colormap """
class VectorColorMap2D(Visualization2D):
    def __init__(self, fdata, vdata, rootpath='.'):
        super(VectorColorMap2D, self).__init__([fdata, vdata], rootpath)

        if scivis.options['plot.label.cbar'] is not None:
            self.cbar_label = scivis.options['plot.label.cbar']
        else:
            self.cbar_label = fdata.label

        self.nframes = 0
        self.scale = 0
        self.vecscale = 0

    def plot_plane(self):
        itfill = 9

        self.nframes = float(self.compute_nframes())

        # Compute the scale of the plot
        if isinstance(scivis.options['plot.range'], tuple):
            self.scale = scivis.options['plot.range']
        elif scivis.options['plot.range'] == 'auto':
            self.scale = self.compute_scale(self.data[0])
        else:
            self.scale = None

        if isinstance(scivis.options['plot.vecscale'], float):
            vecscale = scivis.options['plot.vecscale']
        elif scivis.options['plot.vecscale'] == 'auto':
            norm = self.compute_norm(self.data[1])
            if norm > 0:
                self.vecscale = norm
            else:
                self.vecscale = 1.0
        else:
            self.vecscale = None

        # plot data buffer
        plot_data = []
        # create processing pool
        global counter
        counter = Value('d', 0.0)
        if scivis.options['base.procs'] > 1:
            procs_pool = Pool(processes=scivis.options['base.procs'])
        else:
            procs_pool = None

        for it in CarpetHDF52D.dataset.iterations:
            # Select the refinement levels to plot and read the data
            rlevel = set(CarpetHDF52D.dataset.select_reflevels(iteration=it)
                    ).intersection(self.wanted_rlevel)
            if len(rlevel) == 0:
                pass
            rlevel = sorted(list(rlevel))

            # Get coordinates and data and apply symmetry/coordinate
            # transformations if needed
            c_orig = self.coord.get_coord(it)
            c_plot = [self.coord.transf_coord(c) for c in c_orig
                    if c is not None]
            g_orig = self.coord.get_cells(it)
            g_plot = [self.coord.transf_coord(g) for g in g_orig
                    if g is not None]
            f_orig = self.data[0].get_grid_data(it)
            f_plot = [self.coord.transf_scalar(c_orig[i], f_orig[i])
                    for i in range(len(f_orig))
                    if c_orig[i] is not None and f_orig[i] is not None]
            v_orig = self.data[1].get_grid_data(it)
            v_plot = [self.coord.transf_vector(c_orig[i], v_orig[i])
                    for i in range(len(v_orig))
                    if c_orig[i] is not None and v_orig[i] is not None]
            if scivis.options["plot.horizons"]:
                bh_orig = self.horizon.get_shapes(self.plane, it)
                bh_plot = [self.horizon.transf_shape(bh) for bh in bh_orig]
            else:
                bh_plot = []

            # Find the current time and convert to ms if needed
            time  = CarpetHDF52D.dataset.get_time(it)
            if scivis.options['base.units'] == 'metric':
                time  = units.conv_time(units.cactus, units.metric, time)*1000
                title = r'$t = %7.3f\ [\mathrm{ms}]$' % time
            else:
                title = r'$t = %10.2f\ [M_\odot]$' % time

            # Push data for plotting into a list
            itstr = str(it).zfill(itfill)

            if scivis.options['base.procs'] > 1:
                plot_data.append([rlevel, itstr, c_plot, g_plot, f_plot,
                    v_plot, bh_plot, title])
                if len(plot_data) >= scivis.options['base.buffer']:
                    # push plotting data to subroutines
                    procs_pool.map(self.plot_sub, plot_data)
                    # clear list
                    del plot_data[:]
                    # collect garbage
                    collect()
            else:
                self.plot_sub([rlevel, itstr, c_plot, g_plot, f_plot,
                    v_plot, bh_plot, title])

        if scivis.options["base.procs"] > 1:
            # plot last iterations
            procs_pool.map(self.plot_sub, plot_data)

        utils.printer.final("---> {0} {1} : plots done!".format(self.root,
            self.plane))

    def plot_sub(self, data):
        """
        Plotting subroutine which gets called in parallel

        NOTE: This is not intended to be used by the final user.
        """
        rlevel, itstr, c_plot, g_plot, f_plot, v_plot, bh_plot, title = data

        global counter
        if scivis.options['base.reflevel'] == "merge" or\
                (scivis.options['base.reflevel'] == "full" and
                        len(self.wanted_rlevel) > 1):
            fname = self.basepath + "/rM/{0}.png".format(itstr)
            utils.printer.progress("---> {0} {1} : {2} ({3} %)".format(
                self.root, self.plane, fname,
                int(counter.value/self.nframes*100)))

            # Plot
            ax = plot.axes()
            im = plot.mycolorplot(ax, c_plot, g_plot, f_plot, self.scale)
            qv, sc = plot.myquiver(ax, c_plot, v_plot, self.vecscale,
                    scivis.options['plot.veczoom'])
            plot.myaxis(ax, c_plot[0], scivis.options['base.axis'],
                    self.plane)
            for bh in bh_plot:
                plot.horizon(ax, bh)

            # Colorbar
            if scivis.options['plot.scale'] == 'linear':
                cb = plot.colorbar(im, format=plot.ScalarFormatter(),
                        ticks=scivis.options['plot.cbar.ticks'])
            else:
                cb = plot.colorbar(im, format=plot.LogFormatterMathtext(),
                        ticks=scivis.options['plot.cbar.ticks'])
            cb.set_label(self.cbar_label)

            # Labels
            ax.set_title(title)
            ax.set_xlabel(self.coord.get_label('x'))
            ax.set_ylabel(self.coord.get_label('y'))
            plot.savefig(fname)
            plot.close()

            # Increment counter
            with counter.get_lock():
                counter.value += 1.0
        if scivis.options['base.reflevel'] != 'merge':
            for idx in range(len(rlevel)):
                rl = rlevel[idx]
                fname = self.basepath + "/r{0}/{1}.png".format(rl, itstr)
                utils.printer.progress("---> {0} {1} : {2} ({3} %)".format(
                    self.root, self.plane, fname,
                    int(counter.value/self.nframes*100)))

                # Plot
                ax = plot.axes()
                im = plot.mycolorplot(ax, [c_plot[rl]], [g_plot[rl]],
                        [f_plot[rl]], self.scale)
                qv, sc = plot.myquiver(ax, [c_plot[rl]], [v_plot[rl]],
                        self.vecscale, scivis.options['plot.veczoom'])
                plot.myaxis(ax, c_plot[rl], scivis.options['base.axis'],
                        self.plane)
                for bh in bh_plot:
                    plot.horizon(ax, bh)

                # Colorbar
                if scivis.options['plot.scale'] == 'linear':
                    cb = plot.colorbar(im, format=plot.ScalarFormatter(),
                            ticks=scivis.options['plot.cbar.ticks'])
                else:
                    cb = plot.colorbar(im, format=plot.LogFormatterMathtext(),
                            ticks=scivis.options['plot.cbar.ticks'])
                cb.set_label(self.cbar_label)

                # Labels
                ax.set_title(title)
                ax.set_xlabel(self.coord.get_label('x'))
                ax.set_ylabel(self.coord.get_label('y'))
                plot.savefig(fname)
                plot.close()

                # Increment counter
                with counter.get_lock():
                    counter.value += 1.0
