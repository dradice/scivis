from scivis.__version__ import __version__

import os
__ROOT__    = os.path.abspath(os.path.dirname(__file__))

from scivis.config import options
