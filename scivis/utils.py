from __future__ import print_function

from math import sqrt
from multiprocessing import Value
import shutil
import os
import sys

def install(src, dst):
    """
    Copies src to dst if dst does not exist
    """
    if not os.path.exists(dst):
        shutil.copy(src, dst)

def mkdir(path):
    """
    Creates a directory if it does not exist
    """
    if not os.path.exists(path):
        os.mkdir(path)

class Ellipse(object):
    """
    A simple class representing an ellipse
    """
    def __init__(self, center=(0., 0.), diam=(1., 1.)):
        self.center = tuple(center)
        self.diam = tuple(diam)
    def scale(self, fac):
        center = (fac*c for c in self.center)
        diam = (fac*d for d in self.diam)
        return Ellipse(center, diam)

class Ellipsoid(object):
    """
    A simple class representing an ellipsoid
    """
    def __init__(self, center=(0., 0., 0.), diam=(1., 1., 1.)):
        self.center = tuple(center)
        self.diam = tuple(diam)
    def slice(self, s=0., axis=0):
        """
        Get a slice of an ellispoid
        * s    : point where to take the slice
        * axis : axis orthogonal to the slicing plane
        If the intersection is empty, this will return None
        """
        tmp = 1. - ((s - self.center[axis])**2)/(self.diam[axis]**2)
        if tmp <= 0:
            return None
        tmp = sqrt(tmp)
        if axis == 0:
            center = (self.center[1], self.center[2])
            diam=(self.diam[1]*tmp, self.diam[2]*tmp)
        elif axis == 1:
            center = (self.center[0], self.center[2])
            diam=(self.diam[0]*tmp, self.diam[2]*tmp)
        elif axis == 2:
            center = (self.center[0], self.center[1])
            diam=(self.diam[0]*tmp, self.diam[1]*tmp)
        else:
            raise ValueError("Invalid axis value: {}".format(axis))
        return Ellipse(center, diam)
    def scale(self, fac):
        center = (fac*c for c in self.center)
        diam = (fac*d for d in self.diam)
        return Ellipsoid(center, diam)

printer_counter = Value('i', 0)
class printer(object):
    """
    Custom printing routines for progess indicators
    """
    @staticmethod
    def progress(s):
        with printer_counter.get_lock():
            if len(s) > printer_counter.value:
                print(('\r' + s), end=' ')
                printer_counter.value = len(s)
            else:
                print(('\r'), end=' ')
                for i in range(printer_counter.value):
                    print((''), end=' ')
                print(('\r' + s), end=' ')
            sys.stdout.flush()

    @staticmethod
    def final(s):
        with printer_counter.get_lock():
            printer.progress(s)
            print('')
            printer_counter.value = 0
            sys.stdout.flush()

def tex_format_real(fmt, x):
    s = fmt % x
    return s.replace(' ', r'\ ')

