import numpy
numpy.seterr(all='ignore')

from numpy import inf

options = {
    'base.datapath'                    : "../",
    'base.procs'                       : 1,
    'base.buffer'                      : 100,
    'base.axis'                        : [-inf, inf, -inf, inf, -inf, inf],
    'base.coordinates'                 : "cartesian",
    'base.makemovies'                  : True,
    'base.vcodec'                      : "mpeg4",
    'base.reflevel'                    : "full",
    'base.levels'                      : [0],
    'base.symmetries'                  : [],
    'base.units'                       : 'cactus',
    'base.continue'                    : True,
    'array.coordinates'                : 'THC_Leakage',
    'array.coordinates.rmax'           : 1.0,
    'plot.interpolation'               : 'nearest',
    'plot.planes'                      : ["xy"],
    'plot.type'                        : 'pcolor',
    'plot.range'                       : 'auto',
    'plot.scale'                       : 'linear',
    'plot.nlevels'                     : 30,
    'plot.tracers'                     : False,
    'plot.tracers.angle'               : 5.0,
    'plot.tracers.color'               : 'black',
    'plot.tracers.size'                : 1,
    'plot.horizons'                    : True,
    'plot.horizons.color'              : 'black',
    'plot.horizons.alpha'              : 1.0,
    'plot.veccolor'                    : 'blue',
    'plot.vecscale'                    : 'auto',
    'plot.veczoom'                     : 1.0,
    'plot.cbar.ticks'                  : None,
    'plot.label.cbar'                  : None,
    'plot.label.xy.x'                  : None,
    'plot.label.xy.y'                  : None,
    'plot.label.xz.x'                  : None,
    'plot.label.xz.y'                  : None,
    'plot.label.yz.x'                  : None,
    'plot.label.yz.y'                  : None,
    'vector.average'                   : 0
}

def validate():
    assert options['base.procs'] > 0
    assert options['base.buffer'] > 0
    assert ((options['base.vcodec'] == "mpeg4") or
            (options['base.vcodec'] == "h264"))
    assert options['base.coordinates'] in ["cartesian", "spherical"]
    assert options['base.reflevel'] in ["all", "full", "merge", "given"]
    assert options['base.units'] in ['cactus', 'metric']
    assert isinstance(options['base.continue'], bool)

    assert options['array.coordinates'] == 'THC_Leakage'
    assert options['array.coordinates.rmax'] > 0.0

    for f in options['base.symmetries']:
        assert f in ['reflecting_xy', 'rotating_180']
        # No symmetries are supported in spherical coordinates
        assert options['base.coordinates'] == 'cartesian'

    for p in options['plot.planes']:
        assert p in ["xy", "xz", "yz"]

    assert options["plot.type"] in ["contour", "pcolor"]

    if not isinstance(options['plot.range'], tuple):
        assert options['plot.range'] in ["auto", "frame"]

    assert options['plot.scale'] in ['linear', 'log', 'log_abs', 'simlog']

    assert isinstance(options['plot.nlevels'], int)

    assert isinstance(options['plot.tracers'], bool)
    assert isinstance(options['plot.tracers.angle'], float)
    assert isinstance(options["plot.tracers.size"], int)

    assert isinstance(options["plot.horizons"], bool)
    assert isinstance(options["plot.horizons.alpha"], float)
    assert 0. <= options["plot.horizons.alpha"] <= 1.

    if not isinstance(options['plot.vecscale'], float):
        assert options['plot.vecscale'] in ["auto", "frame"]

    assert options['plot.cbar.ticks'] is None or \
            isinstance(options['plot.cbar.ticks'], list)
