"""
Module for handling tabulated equations of state

Currently this module only supports tabulated nuclear equation of states
"""

import numpy as np
import h5py
import scivis.units as ut
from scipy.interpolate import RegularGridInterpolator
import os

""" A tabulated nuclear equation of state """
class EOSTable(object):
    def __init__(self):
        """
        Create an empty table
        """
        self.log_rho = None
        self.log_temp = None
        self.ye = None
        self.table = {}
        self.interp = {}

    def read_table(self, fname):
        """
        Initialize the EOS object from a table file

        * fname : must be the filename of a table in EOS_Thermal format
        """
        assert os.path.isfile(fname)

        dfile = h5py.File(fname, "r")
        for k in list(dfile.keys()):
            self.table[k] = np.array(dfile[k])
        del dfile

        self.log_rho = np.log10(self.table["density"])
        self.log_temp = np.log10(self.table["temperature"])
        self.ye = self.table["ye"]

    def get_names(self):
        return list(self.table.keys())

    def evaluate(self, prop, rho, temp, ye):
        """
        * prop  : name of the thermodynamical quantity to compute
        * rho   : rest mass density (in Msun^-2)
        * temp  : temperature (in MeV)
        * ye    : electron fraction
        """
        assert prop in self.table
        assert self.log_rho is not None
        assert self.log_temp is not None
        assert self.ye is not None

        assert rho.shape == temp.shape
        assert temp.shape == ye.shape

        log_rho = np.log10(ut.conv_dens(ut.cactus, ut.cgs, rho))
        log_temp = np.log10(temp)
        xi = np.array((ye.flatten(), log_temp.flatten(),
            log_rho.flatten())).transpose()

        if prop not in self.interp:
            self.interp[prop] = RegularGridInterpolator(
                (self.ye, self.log_temp, self.log_rho), self.table[prop],
                method="linear", bounds_error=False, fill_value=None)

        return self.interp[prop](xi).reshape(rho.shape)

__eos_table__ = EOSTable()

def init(fname):
    __eos_table__.read_table(fname)

def get_names():
    return __eos_table__.get_names()

def evaluate(prop, rho, temp, ye):
    return __eos_table__.evaluate(prop, rho, temp, ye)
