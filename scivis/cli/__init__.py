from scivis.__version__ import __version__

# Import abstract CLI interface
from scivis.cli.command import *

# Import all the CLI commands
from scivis.cli.help import *
from scivis.cli.init import *
from scivis.cli.install import *
from scivis.cli.list import *
