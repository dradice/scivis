import scivis.cli.command as command
import scivis
import scivis.utils

import os
import shutil
import sys

class Init(command.Abstract):
    """
    Initializes the current working directory
    """
    name    = "init"
    desc    = "Initializes the current working directory"
    helpstr = """\
Usage: scivis init

Initializes the current directory structure
"""

    def mkmake(self):
        """
        Generate a Makefile for scivis and return it as a string
        """
        s = """\
PYTHON  = {0}
PYOPTN  = -u -W ignore
SHELL   = /bin/bash

SCRDIR  = scripts
LOGDIR  = log
OUTDIR  = output

SCRIPTS = $(wildcard $(SCRDIR)/*.py)
TARGETS = $(notdir $(SCRIPTS:.py=))

.PHONY: all $(TARGETS)

all: $(TARGETS)

clean:
\t-rm -rf $(OUTDIR)/*

$(TARGETS): %: $(addprefix $(SCRDIR)/, %.py)
\t$(PYTHON) $(PYOPTN) $< 2>&1 | tee $(LOGDIR)/$@.out
""".format(sys.executable)
        return s

    def run(self, args):
        scivis.utils.mkdir("./analysis")
        scivis.utils.mkdir("./analysis/bin")
        scivis.utils.mkdir("./analysis/config")
        scivis.utils.mkdir("./analysis/log")
        scivis.utils.mkdir("./analysis/scripts")
        scivis.utils.mkdir("./analysis/output")

        binpath = os.path.dirname(sys.argv[0])
        shutil.copy(binpath + '/scivis-ffmpeg', './analysis/bin')
        shutil.copy(binpath + "/scivis-h5-extract", "./analysis/bin")
        shutil.copy(binpath + "/scivis-h5-repack", "./analysis/bin")

        # Patch scivis-h5-extract to use the current version of python
        L = open("./analysis/bin/scivis-h5-extract", "r").readlines()
        L[0] = "#!/usr/bin/env {0}\n".format(sys.executable)
        open("./analysis/bin/scivis-h5-extract", "w").writelines(L)

        s = self.mkmake()
        f = open("./analysis/Makefile", "w")
        f.write(s)
        f.close()

        f = open("./analysis/config/__init__.py", "w")
        f.write("__version__ = {0}".format(scivis.__version__))
        f.close()

command.register(Init())
