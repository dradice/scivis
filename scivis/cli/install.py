from __future__ import print_function

import scivis.cli.command as command
from difflib import get_close_matches

import scivis
import scivis.utils as utils
import scivis.mod

import os
import re

class Install(command.Abstract):
    """
    Import one or more scripts into the current working directory
    """
    name    = "install"
    desc    = "Install scripts into the current working directory"
    helpstr = """\
Usage: scivis install [script1|@group1] [[script2|@group2] ...]

Install the given script/group of scripts into the working directory\
"""

    def add(self, mod):
        """
        Installs a single script
        """
        print(("Install: " + mod.name))

        basecfga = "{0}/cfg/base.cfg".format(scivis.__ROOT__)
        basecfgb = "{0}/config/base.py".format(self.basepath)
        configa  = "{0}/cfg/{1}.cfg".format(scivis.__ROOT__, mod.config)
        configb  = "{0}/config/{1}.py".format(self.basepath, mod.config)
        scripta  = "{0}/mod/{1}.mod".format(scivis.__ROOT__, mod.script)
        scriptb  = "{0}/scripts/{1}.py".format(self.basepath, mod.script)

        utils.install(basecfga, basecfgb)
        utils.install(configa,  configb)
        utils.install(scripta,  scriptb)

    def run(self, args):
        if os.path.exists("./analysis"):
            self.basepath = "./analysis"
        else:
            self.basepath = "."

        if not os.path.isfile(self.basepath + "/Makefile"):
            s = """\
The current directory seems not to be initialized. Did you forget to run
\'scivis init\'?\
"""
            print(s)
            exit(1)
        for a in args:
            if a[0] == '@':
                g = a[1:]

                if g not in scivis.mod.moddict:
                    print(("scivis install: group \'{0}\' is unkown".format(a)))
                    exit(1)

                for mod in scivis.mod.moddict[g]:
                    self.add(mod)
            else:
                try:
                    mod = get_close_matches(a, list(scivis.mod.modlist.keys()))[0]
                except IndexError:
                    print(("scivis install: module \'{0}\' is unkown".format(a)))
                    exit(1)
                self.add(scivis.mod.modlist[mod])

command.register(Install())
