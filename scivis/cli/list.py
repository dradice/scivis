import scivis.cli.command as command

import scivis.mod

class List(command.Abstract):
    """
    List command
    """
    name    = "list"
    desc    = "List all the available analysis scripts"
    helpstr = """\
Usage: scivis list [group]

Lists the available analysis scripts. If a group name is specified only the
scripts within the selected group are listed\
"""

    def run(self, args):
        s  = "Available scripts"

        table = {}
        if len(args) == 0:
            s += "\n\n"
            for mod in list(scivis.mod.modlist.values()):
                table[mod.name] = mod.desc
        else:
            s += " in group " + args[0] + "\n\n"
            if args[0] in scivis.mod.moddict:
                for mod in scivis.mod.moddict[args[0]]:
                    table[mod.name] = mod.desc
            else:
                print(("scivis list: unknown group \'" + args[0] + "\'"))
                exit(1)

        for k in sorted(table.keys()):
            s += "    " + k.ljust(14) + " " + table[k] + "\n"
        print(s)

        s = "Available groups\n   "
        for k in sorted(list(scivis.mod.moddict.keys())):
            s += " @{0}".format(k)
        print(s)

command.register(List())
