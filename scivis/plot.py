import abc
from copy import deepcopy
from math import log10, pi, sin

import scivis

from matplotlib import rcParams
from matplotlib.colors import Normalize, LogNorm, SymLogNorm
from matplotlib.patches import Ellipse
from matplotlib.pyplot import *
from matplotlib.ticker import LogFormatterMathtext
import numpy

import sys

def horizon(ax, shape):
    """
    Plots an apparent horizon

    * ax         : a matplotlib.pyplot.axes()
    * shape      : a utils.Ellipse object
    """
    artist = Ellipse(xy=shape.center, width=shape.diam[1], height=shape.diam[0],
            edgecolor=scivis.options["plot.horizons.color"],
            facecolor=scivis.options["plot.horizons.color"],
            alpha=scivis.options["plot.horizons.alpha"])
    ax.add_artist(artist)

def mycolorplot(ax, clist, glist, flist, scale=None):
    """
    plots a scalar field in 2D

    * ax         : a matplotlib.pyplot.axes()
    * clist      : list of coordinates (one tuple for each reflevel)
    * glist      : list of grid cells (one tuple for each reflevel)
    * flist      : list of grid functions (one for each reflevel)
    * scale      : optional: tuple with the scale
    """
    if scivis.options["plot.type"] == "contour":
        return mycontourf(ax, clist, flist, scale)
    elif scivis.options["plot.type"] == "pcolor":
        return mypcolormesh(ax, glist, flist, scale)
    else:
        raise ValueError("Unkown plot type: " +
                str(scivis.options["plot.type"]))

def mycontourf(ax, clist, flist, scale=None):
    """
    ax.contourf wrapper

    * ax         : a matplotlib.pyplot.axes()
    * clist      : list of coordinates (one tuple for each reflevel)
    * flist      : list of grid functions (one for each reflevel)
    * scale      : optional: tuple with the scale
    """
    assert len(clist) == len(flist)

    if scale is None:
        scale = (flist[-1].min(), flist[-1].max())
    else:
        if scale[0] is None:
            scale = (flist[-1].min(), scale[1])
        if scale[1] is None:
            scale = (scale[0], flist[-1].max())

    if scivis.options['plot.scale'] == 'log' or\
            scivis.options['plot.scale'] == 'log_abs':
        scale = max(scale[0], sys.float_info.epsilon), \
                max(scale[1], 2*sys.float_info.epsilon)
        norm = LogNorm(vmin=scale[0], vmax=scale[1])
    elif scivis.options['plot.scale'] == 'symlog':
        norm = SymLogNorm(vmin=scale[0], vmax=scale[1])
    else:
        norm = Normalize(vmin=scale[0], vmax=scale[1])

    if scivis.options['plot.scale'] == 'linear':
        levels = np.arange(scale[0], scale[1],
                (scale[1]-scale[0])/(scivis.options['plot.nlevels'] + 1))
    if scivis.options['plot.scale'] == 'log' or\
            scivis.options['plot.scale'] == 'log_abs':
        log_scale = log10(scale[0]), log10(scale[1])
        try:
            levels = 10.0**(np.arange(log_scale[0], log_scale[1],
                (log_scale[1]-log_scale[0])/(scivis.options['plot.nlevels'] + 1)))
        except ZeroDivisionError:
            levels = None

    for i in range(len(clist)):
        x, y  = clist[i]
        field = flist[i]

        if scivis.options['plot.scale'] == 'log_abs':
            field = np.abs(field)
            field.mask = np.logical_or(field.mask, field == 0)
        if scivis.options['plot.scale'] == 'log':
            field.mask = np.logical_or(field.mask, field <= 0)

        try:
            im = ax.contourf(x, y, field, levels, norm=norm)
        except:
            return None

    return im

def mypcolormesh(ax, clist, flist, scale=None):
    """
    ax.pcolormesh wrapper

    * ax         : a matplotlib.pyplot.axes()
    * clist      : list of grid cells (one tuple for each reflevel)
    * flist      : list of grid functions (one for each reflevel)
    * scale      : optional: tuple with the scale
    """
    assert len(clist) == len(flist)

    if scale is None:
        scale = (flist[-1].min(), flist[-1].max())
    else:
        if scale[0] is None:
            scale = (flist[-1].min(), scale[1])
        if scale[1] is None:
            scale = (scale[0], flist[-1].max())

    if scivis.options['plot.scale'] == 'log' or\
            scivis.options['plot.scale'] == 'log_abs':
        scale = max(scale[0], sys.float_info.epsilon), \
                max(scale[1], 2*sys.float_info.epsilon)
        norm = LogNorm(vmin=scale[0], vmax=scale[1])
    elif scivis.options['plot.scale'] == 'symlog':
        norm = SymLogNorm(vmin=scale[0], vmax=scale[1])
    else:
        norm = Normalize(vmin=scale[0], vmax=scale[1])

    for i in range(len(clist)):
        x, y  = clist[i]
        field = flist[i]

        if scivis.options['plot.scale'] == 'log_abs':
            field = np.abs(field)

        im = ax.pcolormesh(x, y, field, norm=norm, edgecolors='None')

    return im

def myaxis(ax, xy, axis3d, plane):
    """
    ax.axis wrapper

    * ax         : a matplotlib.pyplot.axes()
    * xy         : tuple of arrays with the grid coordinates
    * axis3d     : list with the wanted axis limits in 3D
    * plane      : "xy", "xz" or "yz"
    """
    axis = [[]]*4
    if plane == 'xy':
        axis[0] = max(axis3d[0], xy[0].min())
        axis[1] = min(axis3d[1], xy[0].max())
        axis[2] = max(axis3d[2], xy[1].min())
        axis[3] = min(axis3d[3], xy[1].max())
    elif plane == 'xz':
        axis[0] = max(axis3d[0], xy[0].min())
        axis[1] = min(axis3d[1], xy[0].max())
        axis[2] = max(axis3d[4], xy[1].min())
        axis[3] = min(axis3d[5], xy[1].max())
    elif plane == 'yz':
        axis[0] = max(axis3d[2], xy[0].min())
        axis[1] = min(axis3d[3], xy[0].max())
        axis[2] = max(axis3d[4], xy[1].min())
        axis[3] = min(axis3d[5], xy[1].max())
    else:
        raise Exception("Unkown plane \'{0}\'".format(plane))
    ax.axis(axis)
    ax.set_aspect('equal', 'datalim')

def myquiver(ax, clist, vlist, vecscale=None, veczoom=None):
    """
    ax.quiver wrapper

    * ax       : a matplotlib.pyplot.axes()
    * clist    : list of coordinates (one tuple for each reflevel)
    * vlist    : list of tuples [(vx, vy)_rl] of masked arrays
    * vecscale : (optional) scale of the vectors
    * veczoom  : (optional) vector scaling factors

    This returns the quiverplot and the effective vecscale
    """
    assert len(clist) == len(vlist)

    if vecscale is None:
        x, y   = clist[-1]
        vx, vy = vlist[-1]
        norm = np.ma.masked_invalid(np.sqrt(vx*vx + vy*vy)).max()
        if norm > 0:
            vecscale = norm
        else:
            vecscale = 1.0

    if veczoom is not None:
        vecscale = vecscale/veczoom

    for i in range(len(clist)):
        x, y   = clist[i]
        vx, vy = vlist[i]

        vx = vx/vecscale
        vy = vy/vecscale

        x = numpy.ma.masked_array(x, mask=vx.mask)
        y = numpy.ma.masked_array(y, mask=vy.mask)

        im = ax.quiver(x, y, vx, vy, angles='xy', scale_units='xy',
                scale=1, units='xy', color=scivis.options['plot.veccolor'])

    return im, vecscale
