import re
import tempfile
import os

import scivis

""" Make a movie out of all the .PNG in a given directory using ffmpeg """
def ffmpeg(dirname):
    ldir   = tempfile.mkdtemp()
    fnames = os.listdir(dirname)
    fnames = [f for f in fnames if re.match(r".*\.png$", f) is not None]
    fnames = sorted(fnames)

    if scivis.options['base.vcodec'] == "mpeg4":
        os.system("ffmpeg -y -pattern_type glob -i \'{0}/*.png\' -vcodec mpeg4 {1}/movie.mp4"
            " &> /dev/null".format(dirname, dirname))
    if scivis.options['base.vcodec'] == 'h264':
        os.system("ffmpeg -threads 0 -framerate 25 -pattern_type glob -i \'{0}/*.png\' -c:v libx264"
            " -pix_fmt yuv420p {1}/movie.mp4 &>> /dev/null".format(dirname, dirname))
